# Article supplementary materials
This repository contains [SAS script](em.sas) with statistical analyses performed for the purpose of the following article:

Loboda T.D., Brusilovsky, P., & Brunstein, J. (2011) Inferring word relevance from eye-movements of readers.  _Intelligent User Interfaces (IUI)_, 175-184. [[author's copy](iui-11-loboda.pdf)]


## License
This project is licensed under the [BSD License](LICENSE.md).
