/*
 * Data set legend:
 *   roi  : by ROI
 *   sseq : by stim-seq
 *   A    : all data (i.e., disregard marking)
 *   m    : marked data (separation into marked and non-marked)
 *   ps   : perceptual span
 *   a    : flag: adjusted EMs?
 *   80   : only fixations with duration of 80-1200 ms
 *   prob : only probabilities
 *   cls  : classifier
 *   ev   : evaluation
 *   dc   : decision criterion (criteria)
 */




* --- INIT ---;

%let DATA_DIR=T:\word-relev\;
*%let MIN_NF_NROI_RATIO=0.4;

ods html style=Styles.Journal gpath="&DATA_DIR\out\";



* --- DATA ---;


* By ROI;
data D_roi_ps1_a1_80;
	infile "&DATA_DIR\data-roi-ps1-a1-80.txt" expandtabs firstobs=2 lrecl=2048;
	input sub trial$ stim$ em_nevt em_dur em_roi :$16. em_nf em_nfpf em_nr em_nfpr em_wlen em_afd em_ffd em_sfd em_gd em_tt em_rp mrc_roi :$14. mrc_n mrc_nlet mrc_nphon mrc_nsyl mrc_kffrq mrc_kffrq_ln mrc_kffrq_log10 mrc_kfcat mrc_kfsmp mrc_tlfrq mrc_tlfrq_ln mrc_tlfrq_log10 mrc_bfrq mrc_bfrq_ln mrc_bfrq_log10 mrc_fam mrc_cnc mrc_img mrc_meanc mrc_meanp mrc_aoa roi_cl_e roi_cl_s roi_ln_e roi_ln_s roi_marked roi_pg_e roi_pg_s roi_sent_e roi_sent_s;

	m = roi_marked;
	se = roi_sent_e;

	em_roi_norm = compress(strip(em_roi), ",.;'()1234567890");
	em_wlen_norm = lengthn(em_roi_norm);

	if (em_nf > 0)  then em_nf_nz = 1;  else em_nf_nz = 0;
	if (em_nf = 0)  then em_nf_0  = 1;  else em_nf_0  = 0;
	if (em_nf = 1)  then em_nf_1  = 1;  else em_nf_1  = 0;
	if (em_nf >= 2) then em_nf_2  = 1;  else em_nf_2  = 0;

	if (em_nfpf > 0)  then em_nfpf_nz = 1;  else em_nfpf_nz = 0;

	if (em_wlen_norm >= 1 and em_wlen_norm <= 3) then em_wlen_norm_sml = 0;
	else if (em_wlen_norm >= 4 and em_wlen_norm <= 6) then em_wlen_norm_sml = 1;
	else if (em_wlen_norm >= 7) then em_wlen_norm_sml = 2;
	else em_wlen_norm_sml = .;

	if (mrc_kffrq_log10 > 0 and mrc_kffrq_log10 < 1) then mrc_kffrq_log10_disc = 1;
	else if (mrc_kffrq_log10 >= 1 and mrc_kffrq_log10 < 2) then mrc_kffrq_log10_disc = 2;
	else if (mrc_kffrq_log10 >= 2 and mrc_kffrq_log10 < 3) then mrc_kffrq_log10_disc = 3;
	else if (mrc_kffrq_log10 >= 3 and mrc_kffrq_log10 < 4) then mrc_kffrq_log10_disc = 4;
	else if (mrc_kffrq_log10 >= 4 and mrc_kffrq_log10 < 5) then mrc_kffrq_log10_disc = 5;
	else if (mrc_kffrq_log10 >= 5) then mrc_kffrq_log10_disc = 6;
	else mrc_kffrq_log10_disc = .;

	if (mrc_kffrq_log10 > 0 and mrc_kffrq_log10 < 3) then mrc_kffrq_log10_lh = 0;
	else if (mrc_kffrq_log10 >= 3) then mrc_kffrq_log10_lh = 1;
	else mrc_kffrq_log10_disc = .;

	if (mrc_aoa >= 100 and mrc_aoa < 200) then mrc_aoa_disc = 1;
	else if (mrc_aoa >= 200 and mrc_aoa < 300) then mrc_aoa_disc = 2;
	else if (mrc_aoa >= 300 and mrc_aoa < 400) then mrc_aoa_disc = 3;
	else if (mrc_aoa >= 400 and mrc_aoa < 500) then mrc_aoa_disc = 4;
	else if (mrc_aoa >= 500 and mrc_aoa < 600) then mrc_aoa_disc = 5;
	else if (mrc_aoa >= 600) then mrc_aoa_disc = 6;
	else mrc_aoa_disc = .;

	if (mrc_fam >= 100 and mrc_fam < 200) then mrc_fam_disc = 1;
	else if (mrc_fam >= 200 and mrc_fam < 300) then mrc_fam_disc = 2;
	else if (mrc_fam >= 300 and mrc_fam < 400) then mrc_fam_disc = 3;
	else if (mrc_fam >= 400 and mrc_fam < 500) then mrc_fam_disc = 4;
	else if (mrc_fam >= 500 and mrc_fam < 600) then mrc_fam_disc = 5;
	else if (mrc_fam >= 600) then mrc_fam_disc = 6;
	else mrc_fam_disc = .;
run;
proc print data=D_roi_ps1_a1_80 (obs=6); run;


* By ROI: Prior effects;
proc tabulate data=D_roi_ps1_a1_80 out=D_roi_ps1_a1_80_prior_eff;
	class em_wlen_norm;
	var em_nf em_nfpf em_ffd em_sfd em_gd em_tt;
	table em_wlen_norm,em_nf em_nfpf em_ffd em_sfd em_gd em_tt;
run;


* By ROI: probabilities;
proc tabulate data=D_roi_ps1_a1_80 out=D_roi_ps1_a1_80_prob;
	class sub m se em_wlen_norm mrc_kffrq_log10_disc;
	var em_nf_nz em_nf_0 em_nf_1 em_nf_2 em_nr em_nfpr;
	table sub*m*se*em_wlen_norm*mrc_kffrq_log10_disc,em_nf_nz em_nf_0 em_nf_1 em_nf_2 em_nr em_nfpr;
run;

data D_roi_ps1_a1_80_prob;
	set D_roi_ps1_a1_80_prob;

	n = em_nf_0_Sum + em_nf_1_Sum + em_nf_2_Sum;

	rename em_nf_nz_Sum=em_nf_nz em_nf_0_Sum=em_nf_0 em_nf_1_Sum=em_nf_1 em_nf_2_Sum=em_nf_2 em_nr_Sum=em_nr em_nfpr_Sum=em_nfpr;
	drop _TYPE_ _PAGE_ _TABLE_;
run;

proc sort data=D_roi_ps1_a1_80_prob; by sub m se em_wlen_norm mrc_kffrq_log10_disc; run;
proc print data=D_roi_ps1_a1_80_prob (obs=50); run;


* Prior effects;
data D_prior_eff_ps1_a1_80;
	infile "&DATA_DIR\data-prior-eff-ps1-a1-80.txt" expandtabs firstobs=2 lrecl=2048;
	input x len_cnt len_nf_m len_nf_se len_nf_l95 len_nf_u95 len_nfpf_m len_nfpf_se len_nfpf_l95 len_nfpf_u95 len_sfd_m len_sfd_se len_sfd_l95 len_sfd_u95 len_ffd_m len_ffd_se len_ffd_l95 len_ffd_u95 len_gd_m len_gd_se len_gd_l95 len_gd_u95 len_tt_m len_tt_se len_tt_l95 len_tt_u95 frq_cnt frq_nf_m frq_nf_se frq_nf_l95 frq_nf_u95 frq_nfpf_m frq_nfpf_se frq_nfpf_l95 frq_nfpf_u95 frq_sfd_m frq_sfd_se frq_sfd_l95 frq_sfd_u95 frq_ffd_m frq_ffd_se frq_ffd_l95 frq_ffd_u95 frq_gd_m frq_gd_se frq_gd_l95 frq_gd_u95 frq_tt_m frq_tt_se frq_tt_l95 frq_tt_u95 aoa_cnt aoa_nf_m aoa_nf_se aoa_nf_l95 aoa_nf_u95 aoa_nfpf_m aoa_nfpf_se aoa_nfpf_l95 aoa_nfpf_u95 aoa_sfd_m aoa_sfd_se aoa_sfd_l95 aoa_sfd_u95 aoa_ffd_m aoa_ffd_se aoa_ffd_l95 aoa_ffd_u95 aoa_gd_m aoa_gd_se aoa_gd_l95 aoa_gd_u95 aoa_tt_m aoa_tt_se aoa_tt_l95 aoa_tt_u95 fam_cnt fam_nf_m fam_nf_se fam_nf_l95 fam_nf_u95 fam_nfpf_m fam_nfpf_se fam_nfpf_l95 fam_nfpf_u95 fam_sfd_m fam_sfd_se fam_sfd_l95 fam_sfd_u95 fam_ffd_m fam_ffd_se fam_ffd_l95 fam_ffd_u95 fam_gd_m fam_gd_se fam_gd_l95 fam_gd_u95 fam_tt_m fam_tt_se fam_tt_l95 fam_tt_u95;
run;
proc print data=D_prior_eff_ps1_a1_80 (obs=3); run;


* Classifier (len: SML, frq: 1..6);
data D_cls_l3_f6_ps1_a1_80;
	infile "&DATA_DIR\data-class-len3-frq6-ps1-a1-80.txt" expandtabs firstobs=2 lrecl=2048;
	input frq se0_m0_l0_nf_nz_m se0_m0_l0_nf_nz_se se0_m0_l1_nf_nz_m se0_m0_l1_nf_nz_se se0_m0_l2_nf_nz_m se0_m0_l2_nf_nz_se se0_m0_l0_nfpf_nz_m se0_m0_l0_nfpf_nz_se se0_m0_l1_nfpf_nz_m se0_m0_l1_nfpf_nz_se se0_m0_l2_nfpf_nz_m se0_m0_l2_nfpf_nz_se se0_m0_l0_tt_m se0_m0_l0_tt_se se0_m0_l1_tt_m se0_m0_l1_tt_se se0_m0_l2_tt_m se0_m0_l2_tt_se se0_m1_l0_nf_nz_m se0_m1_l0_nf_nz_se se0_m1_l1_nf_nz_m se0_m1_l1_nf_nz_se se0_m1_l2_nf_nz_m se0_m1_l2_nf_nz_se se0_m1_l0_nfpf_nz_m se0_m1_l0_nfpf_nz_se se0_m1_l1_nfpf_nz_m se0_m1_l1_nfpf_nz_se se0_m1_l2_nfpf_nz_m se0_m1_l2_nfpf_nz_se se0_m1_l0_tt_m se0_m1_l0_tt_se se0_m1_l1_tt_m se0_m1_l1_tt_se se0_m1_l2_tt_m se0_m1_l2_tt_se se1_m0_l0_nf_nz_m se1_m0_l0_nf_nz_se se1_m0_l1_nf_nz_m se1_m0_l1_nf_nz_se se1_m0_l2_nf_nz_m se1_m0_l2_nf_nz_se se1_m0_l0_nfpf_nz_m se1_m0_l0_nfpf_nz_se se1_m0_l1_nfpf_nz_m se1_m0_l1_nfpf_nz_se se1_m0_l2_nfpf_nz_m se1_m0_l2_nfpf_nz_se se1_m0_l0_tt_m se1_m0_l0_tt_se se1_m0_l1_tt_m se1_m0_l1_tt_se se1_m0_l2_tt_m se1_m0_l2_tt_se se1_m1_l0_nf_nz_m se1_m1_l0_nf_nz_se se1_m1_l1_nf_nz_m se1_m1_l1_nf_nz_se se1_m1_l2_nf_nz_m se1_m1_l2_nf_nz_se se1_m1_l0_nfpf_nz_m se1_m1_l0_nfpf_nz_se se1_m1_l1_nfpf_nz_m se1_m1_l1_nfpf_nz_se se1_m1_l2_nfpf_nz_m se1_m1_l2_nfpf_nz_se se1_m1_l0_tt_m se1_m1_l0_tt_se se1_m1_l1_tt_m se1_m1_l1_tt_se se1_m1_l2_tt_m se1_m1_l2_tt_se;

	* Standard errors;
	se0_m0_l0_nf_nz_se_l = se0_m0_l0_nf_nz_m - se0_m0_l0_nf_nz_se;
	se0_m0_l0_nf_nz_se_u = se0_m0_l0_nf_nz_m + se0_m0_l0_nf_nz_se;
	se0_m1_l0_nf_nz_se_l = se0_m1_l0_nf_nz_m - se0_m1_l0_nf_nz_se;
	se0_m1_l0_nf_nz_se_u = se0_m1_l0_nf_nz_m + se0_m1_l0_nf_nz_se;
	se0_m0_l1_nf_nz_se_l = se0_m0_l1_nf_nz_m - se0_m0_l1_nf_nz_se;
	se0_m0_l1_nf_nz_se_u = se0_m0_l1_nf_nz_m + se0_m0_l1_nf_nz_se;
	se0_m1_l1_nf_nz_se_l = se0_m1_l1_nf_nz_m - se0_m1_l1_nf_nz_se;
	se0_m1_l1_nf_nz_se_u = se0_m1_l1_nf_nz_m + se0_m1_l1_nf_nz_se;
	se0_m0_l2_nf_nz_se_l = se0_m0_l2_nf_nz_m - se0_m0_l2_nf_nz_se;
	se0_m0_l2_nf_nz_se_u = se0_m0_l2_nf_nz_m + se0_m0_l2_nf_nz_se;
	se0_m1_l2_nf_nz_se_l = se0_m1_l2_nf_nz_m - se0_m1_l2_nf_nz_se;
	se0_m1_l2_nf_nz_se_u = se0_m1_l2_nf_nz_m + se0_m1_l2_nf_nz_se;

	se0_m0_l0_tt_se_l = se0_m0_l0_tt_m - se0_m0_l0_tt_se;
	se0_m0_l0_tt_se_u = se0_m0_l0_tt_m + se0_m0_l0_tt_se;
	se0_m1_l0_tt_se_l = se0_m1_l0_tt_m - se0_m1_l0_tt_se;
	se0_m1_l0_tt_se_u = se0_m1_l0_tt_m + se0_m1_l0_tt_se;
	se0_m0_l1_tt_se_l = se0_m0_l1_tt_m - se0_m0_l1_tt_se;
	se0_m0_l1_tt_se_u = se0_m0_l1_tt_m + se0_m0_l1_tt_se;
	se0_m1_l1_tt_se_l = se0_m1_l1_tt_m - se0_m1_l1_tt_se;
	se0_m1_l1_tt_se_u = se0_m1_l1_tt_m + se0_m1_l1_tt_se;
	se0_m0_l2_tt_se_l = se0_m0_l2_tt_m - se0_m0_l2_tt_se;
	se0_m0_l2_tt_se_u = se0_m0_l2_tt_m + se0_m0_l2_tt_se;
	se0_m1_l2_tt_se_l = se0_m1_l2_tt_m - se0_m1_l2_tt_se;
	se0_m1_l2_tt_se_u = se0_m1_l2_tt_m + se0_m1_l2_tt_se;


	se1_m0_l0_nf_nz_se_l = se1_m0_l0_nf_nz_m - se1_m0_l0_nf_nz_se;
	se1_m0_l0_nf_nz_se_u = se1_m0_l0_nf_nz_m + se1_m0_l0_nf_nz_se;
	se1_m1_l0_nf_nz_se_l = se1_m1_l0_nf_nz_m - se1_m1_l0_nf_nz_se;
	se1_m1_l0_nf_nz_se_u = se1_m1_l0_nf_nz_m + se1_m1_l0_nf_nz_se;
	se1_m0_l1_nf_nz_se_l = se1_m0_l1_nf_nz_m - se1_m0_l1_nf_nz_se;
	se1_m0_l1_nf_nz_se_u = se1_m0_l1_nf_nz_m + se1_m0_l1_nf_nz_se;
	se1_m1_l1_nf_nz_se_l = se1_m1_l1_nf_nz_m - se1_m1_l1_nf_nz_se;
	se1_m1_l1_nf_nz_se_u = se1_m1_l1_nf_nz_m + se1_m1_l1_nf_nz_se;
	se1_m0_l2_nf_nz_se_l = se1_m0_l2_nf_nz_m - se1_m0_l2_nf_nz_se;
	se1_m0_l2_nf_nz_se_u = se1_m0_l2_nf_nz_m + se1_m0_l2_nf_nz_se;
	se1_m1_l2_nf_nz_se_l = se1_m1_l2_nf_nz_m - se1_m1_l2_nf_nz_se;
	se1_m1_l2_nf_nz_se_u = se1_m1_l2_nf_nz_m + se1_m1_l2_nf_nz_se;

	se1_m0_l0_tt_se_l = se1_m0_l0_tt_m - se1_m0_l0_tt_se;
	se1_m0_l0_tt_se_u = se1_m0_l0_tt_m + se1_m0_l0_tt_se;
	se1_m1_l0_tt_se_l = se1_m1_l0_tt_m - se1_m1_l0_tt_se;
	se1_m1_l0_tt_se_u = se1_m1_l0_tt_m + se1_m1_l0_tt_se;
	se1_m0_l1_tt_se_l = se1_m0_l1_tt_m - se1_m0_l1_tt_se;
	se1_m0_l1_tt_se_u = se1_m0_l1_tt_m + se1_m0_l1_tt_se;
	se1_m1_l1_tt_se_l = se1_m1_l1_tt_m - se1_m1_l1_tt_se;
	se1_m1_l1_tt_se_u = se1_m1_l1_tt_m + se1_m1_l1_tt_se;
	se1_m0_l2_tt_se_l = se1_m0_l2_tt_m - se1_m0_l2_tt_se;
	se1_m0_l2_tt_se_u = se1_m0_l2_tt_m + se1_m0_l2_tt_se;
	se1_m1_l2_tt_se_l = se1_m1_l2_tt_m - se1_m1_l2_tt_se;
	se1_m1_l2_tt_se_u = se1_m1_l2_tt_m + se1_m1_l2_tt_se;

	* Binary classification decision criteria (TT only);
	se1_l0_tt_md = se1_m1_l0_tt_m - se1_m0_l0_tt_m;  * md = mean difference;
	se1_l1_tt_md = se1_m1_l1_tt_m - se1_m0_l1_tt_m;
	se1_l2_tt_md = se1_m1_l2_tt_m - se1_m0_l2_tt_m;

	se1_l0_tt_midp = se1_m0_l0_tt_m + se1_l0_tt_md / 2;  * midp = mid-point (between TT for m0 and m1);
	se1_l1_tt_midp = se1_m0_l1_tt_m + se1_l1_tt_md / 2;
	se1_l2_tt_midp = se1_m0_l2_tt_m + se1_l2_tt_md / 2;
run;


* Classifier (len: SML, frq: LH);
data D_cls_l3_f2_ps1_a1_80;
	infile "&DATA_DIR\data-class-len3-frq2-ps1-a1-80.txt" expandtabs firstobs=2 lrecl=2048;
	input frq se0_m0_l0_nf_nz_m se0_m0_l0_nf_nz_se se0_m0_l1_nf_nz_m se0_m0_l1_nf_nz_se se0_m0_l2_nf_nz_m se0_m0_l2_nf_nz_se se0_m0_l0_tt_m se0_m0_l0_tt_se se0_m0_l1_tt_m se0_m0_l1_tt_se se0_m0_l2_tt_m se0_m0_l2_tt_se se0_m1_l0_nf_nz_m se0_m1_l0_nf_nz_se se0_m1_l1_nf_nz_m se0_m1_l1_nf_nz_se se0_m1_l2_nf_nz_m se0_m1_l2_nf_nz_se se0_m1_l0_tt_m se0_m1_l0_tt_se se0_m1_l1_tt_m se0_m1_l1_tt_se se0_m1_l2_tt_m se0_m1_l2_tt_se se1_m0_l0_nf_nz_m se1_m0_l0_nf_nz_se se1_m0_l1_nf_nz_m se1_m0_l1_nf_nz_se se1_m0_l2_nf_nz_m se1_m0_l2_nf_nz_se se1_m0_l0_tt_m se1_m0_l0_tt_se se1_m0_l1_tt_m se1_m0_l1_tt_se se1_m0_l2_tt_m se1_m0_l2_tt_se se1_m1_l0_nf_nz_m se1_m1_l0_nf_nz_se se1_m1_l1_nf_nz_m se1_m1_l1_nf_nz_se se1_m1_l2_nf_nz_m se1_m1_l2_nf_nz_se se1_m1_l0_tt_m se1_m1_l0_tt_se se1_m1_l1_tt_m se1_m1_l1_tt_se se1_m1_l2_tt_m se1_m1_l2_tt_se;

	if (frq = 0) then frq_str = 'L';
	else frq_str = 'H';

	* Standard errors;
	se0_m0_l0_nf_nz_se_l = se0_m0_l0_nf_nz_m - se0_m0_l0_nf_nz_se;
	se0_m0_l0_nf_nz_se_u = se0_m0_l0_nf_nz_m + se0_m0_l0_nf_nz_se;
	se0_m1_l0_nf_nz_se_l = se0_m1_l0_nf_nz_m - se0_m1_l0_nf_nz_se;
	se0_m1_l0_nf_nz_se_u = se0_m1_l0_nf_nz_m + se0_m1_l0_nf_nz_se;
	se0_m0_l1_nf_nz_se_l = se0_m0_l1_nf_nz_m - se0_m0_l1_nf_nz_se;
	se0_m0_l1_nf_nz_se_u = se0_m0_l1_nf_nz_m + se0_m0_l1_nf_nz_se;
	se0_m1_l1_nf_nz_se_l = se0_m1_l1_nf_nz_m - se0_m1_l1_nf_nz_se;
	se0_m1_l1_nf_nz_se_u = se0_m1_l1_nf_nz_m + se0_m1_l1_nf_nz_se;
	se0_m0_l2_nf_nz_se_l = se0_m0_l2_nf_nz_m - se0_m0_l2_nf_nz_se;
	se0_m0_l2_nf_nz_se_u = se0_m0_l2_nf_nz_m + se0_m0_l2_nf_nz_se;
	se0_m1_l2_nf_nz_se_l = se0_m1_l2_nf_nz_m - se0_m1_l2_nf_nz_se;
	se0_m1_l2_nf_nz_se_u = se0_m1_l2_nf_nz_m + se0_m1_l2_nf_nz_se;

	se0_m0_l0_tt_se_l = se0_m0_l0_tt_m - se0_m0_l0_tt_se;
	se0_m0_l0_tt_se_u = se0_m0_l0_tt_m + se0_m0_l0_tt_se;
	se0_m1_l0_tt_se_l = se0_m1_l0_tt_m - se0_m1_l0_tt_se;
	se0_m1_l0_tt_se_u = se0_m1_l0_tt_m + se0_m1_l0_tt_se;
	se0_m0_l1_tt_se_l = se0_m0_l1_tt_m - se0_m0_l1_tt_se;
	se0_m0_l1_tt_se_u = se0_m0_l1_tt_m + se0_m0_l1_tt_se;
	se0_m1_l1_tt_se_l = se0_m1_l1_tt_m - se0_m1_l1_tt_se;
	se0_m1_l1_tt_se_u = se0_m1_l1_tt_m + se0_m1_l1_tt_se;
	se0_m0_l2_tt_se_l = se0_m0_l2_tt_m - se0_m0_l2_tt_se;
	se0_m0_l2_tt_se_u = se0_m0_l2_tt_m + se0_m0_l2_tt_se;
	se0_m1_l2_tt_se_l = se0_m1_l2_tt_m - se0_m1_l2_tt_se;
	se0_m1_l2_tt_se_u = se0_m1_l2_tt_m + se0_m1_l2_tt_se;


	se1_m0_l0_nf_nz_se_l = se1_m0_l0_nf_nz_m - se1_m0_l0_nf_nz_se;
	se1_m0_l0_nf_nz_se_u = se1_m0_l0_nf_nz_m + se1_m0_l0_nf_nz_se;
	se1_m1_l0_nf_nz_se_l = se1_m1_l0_nf_nz_m - se1_m1_l0_nf_nz_se;
	se1_m1_l0_nf_nz_se_u = se1_m1_l0_nf_nz_m + se1_m1_l0_nf_nz_se;
	se1_m0_l1_nf_nz_se_l = se1_m0_l1_nf_nz_m - se1_m0_l1_nf_nz_se;
	se1_m0_l1_nf_nz_se_u = se1_m0_l1_nf_nz_m + se1_m0_l1_nf_nz_se;
	se1_m1_l1_nf_nz_se_l = se1_m1_l1_nf_nz_m - se1_m1_l1_nf_nz_se;
	se1_m1_l1_nf_nz_se_u = se1_m1_l1_nf_nz_m + se1_m1_l1_nf_nz_se;
	se1_m0_l2_nf_nz_se_l = se1_m0_l2_nf_nz_m - se1_m0_l2_nf_nz_se;
	se1_m0_l2_nf_nz_se_u = se1_m0_l2_nf_nz_m + se1_m0_l2_nf_nz_se;
	se1_m1_l2_nf_nz_se_l = se1_m1_l2_nf_nz_m - se1_m1_l2_nf_nz_se;
	se1_m1_l2_nf_nz_se_u = se1_m1_l2_nf_nz_m + se1_m1_l2_nf_nz_se;

	se1_m0_l0_tt_se_l = se1_m0_l0_tt_m - se1_m0_l0_tt_se;
	se1_m0_l0_tt_se_u = se1_m0_l0_tt_m + se1_m0_l0_tt_se;
	se1_m1_l0_tt_se_l = se1_m1_l0_tt_m - se1_m1_l0_tt_se;
	se1_m1_l0_tt_se_u = se1_m1_l0_tt_m + se1_m1_l0_tt_se;
	se1_m0_l1_tt_se_l = se1_m0_l1_tt_m - se1_m0_l1_tt_se;
	se1_m0_l1_tt_se_u = se1_m0_l1_tt_m + se1_m0_l1_tt_se;
	se1_m1_l1_tt_se_l = se1_m1_l1_tt_m - se1_m1_l1_tt_se;
	se1_m1_l1_tt_se_u = se1_m1_l1_tt_m + se1_m1_l1_tt_se;
	se1_m0_l2_tt_se_l = se1_m0_l2_tt_m - se1_m0_l2_tt_se;
	se1_m0_l2_tt_se_u = se1_m0_l2_tt_m + se1_m0_l2_tt_se;
	se1_m1_l2_tt_se_l = se1_m1_l2_tt_m - se1_m1_l2_tt_se;
	se1_m1_l2_tt_se_u = se1_m1_l2_tt_m + se1_m1_l2_tt_se;

	* Binary classification decision criteria (TT only);
	se1_l0_tt_md = se1_m1_l0_tt_m - se1_m0_l0_tt_m;  * md = mean difference;
	se1_l1_tt_md = se1_m1_l1_tt_m - se1_m0_l1_tt_m;
	se1_l2_tt_md = se1_m1_l2_tt_m - se1_m0_l2_tt_m;

	se1_l0_tt_midp = se1_m0_l0_tt_m + se1_l0_tt_md / 2;  * midp = mid-point (between TT for m0 and m1);
	se1_l1_tt_midp = se1_m0_l1_tt_m + se1_l1_tt_md / 2;
	se1_l2_tt_midp = se1_m0_l2_tt_m + se1_l2_tt_md / 2;
run;




* By fixation;
data D_fix_a1_80;
	infile "&DATA_DIR\data-fix-a1-80.txt" expandtabs firstobs=2 lrecl=2048;
	input sub trial$ stim$ ts_i em_roi :$16. em_t em_fd em_wlen em_fp em_sacc_amp em_sacc_dir em_ot_dir em_ot_bin em_os_dir em_os_bin mrc_roi :$14. mrc_n mrc_nlet mrc_nphon mrc_nsyl mrc_kffrq mrc_kffrq_ln mrc_kffrq_log10 mrc_kfcat mrc_kfsmp mrc_tlfrq mrc_tlfrq_ln mrc_tlfrq_log10 mrc_bfrq mrc_bfrq_ln mrc_bfrq_log10 mrc_fam mrc_cnc mrc_img mrc_meanc mrc_meanp mrc_aoa;

	m = roi_marked;
	se = roi_sent_e;

	em_roi_norm = compress(strip(em_roi), ",.;'()1234567890");
	em_wlen_norm = lengthn(em_roi_norm);
run;


/*
data D_sseq_m_ps1_a1_80;
	infile "&DATA_DIR\data-sseq-m-ps1-a1-80.txt" expandtabs firstobs=2 lrecl=2048;
	input m sub trial$ stim$ em_nevt em_dur em_nroi em_nrois em_wpm em_wpms em_nf em_nfpf em_nb em_nr em_nfpr em_wlen em_wlens em_afd em_ffd em_sfd em_gd em_tt em_nwf0 em_nwf0s em_nwf1 em_nwf2 em_nwf em_pwf em_pwfs em_notf em_nosf em_p0 em_p1 em_p2 em_p0s em_p1s em_p2s em_p0fp em_p1fp em_p2fp mrc_nlet mrc_kffrq_log10 mrc_fam mrc_aoa;
run;
proc print data=D_sseq_m_ps1_a1_80 (obs=3); run;
*/




* --- DATA INSIGHT ---;

* Descriptives;
proc means data=D_roi_ps1_a1_80 (where=(em_wlen_norm > 0 and em_wlen_norm <= 11)) maxdec=2;
	*where se = 1;
	*class m;
	where em_wlen > 11;
	var em_nf em_nfpf em_nr em_nfpr;
	var em_afd em_ffd em_sfd em_gd em_tt;
	var em_wlen em_wlen_norm em_wlen_norm_sml mrc_kffrq_log10 mrc_kffrq_log10_disc mrc_aoa mrc_aoa_disc mrc_fam mrc_fam_disc;
	var em_nf_0 em_nf_1 em_nf_2;
run;


proc freq data=D_roi_ps1_a1_80 (where=(em_wlen_norm > 0 and em_wlen_norm <= 11)); by sub; tables m; run;


proc means data=D_roi_ps1_a1_80_prob (where=(em_wlen_norm > 0 and em_wlen_norm <= 11)) maxdec=2;
	class m;
	var em_nf_nz em_nf_0 em_nf_1 em_nf_2;
run;


proc means data=D_sseq_m_ps1_a1_80 maxdec=2;
	where em_nrois >= 50;
	*class m;
	var em_nroi em_nrois em_wpm em_wpms em_nf em_nfpf em_nr em_nfpr em_nwf0 em_nwf0s em_nwf1 em_nwf2 em_nwf;
run;


proc means data=D_fix_a1_80 maxdec=2;
	var em_fd em_sacc_amp em_sacc_dir em_ot_dir em_ot_bin em_os_dir em_os_bin;
run;

proc freq data=D_roi_ps1_a1_80 (where=(em_wlen_norm > 0 and em_wlen_norm <= 11)); tables em_wlen_norm; run;




* Autocorrelation;
* The value of Durbin-Watson statistic is close to 2 if the errors are uncorrelated;
proc sort data=D_fix_a1_80; by sub ts_i; run;
proc reg data=D_fix_a1_80;
	by sub;
	model em_fd = /dw;
run;


proc sort data=D_roi_ps1_a1_80; by sub; run;
proc reg data=D_roi_ps1_a1_80;
	by sub;
	model em_gd em_wlen_norm mrc_kffrq_log10 = /dw;
run;




* Correlations;
proc corr data=D_roi_ps1_a1_80 (where=(em_wlen_norm > 0 and em_wlen_norm <= 13));
	*var em_nf em_nfpf;
	*var em_sfd em_ffd em_gd em_tt;
	var em_p0_bin em_p1_bin em_p2_bin;
run;




* Histogram: fd;

proc template;
	define statgraph HistDur;
	begingraph;
		layout lattice / columns=2 rows=1 columngutter=0 rowgutter=0 skipemptycells=true rowdatarange=union columndatarange=union;
			rowaxes;
				rowaxis / griddisplay=on gridattrs=(pattern=1) display=(label ticks tickvalues) label="%" labelattrs=(weight=bold);
			endrowaxes;

			columnaxes;
				columnaxis / griddisplay=off gridattrs=(pattern=1) display=(label ticks tickvalues) labelattrs=(weight=bold) label="GD" type=linear offsetmin=0.025 offsetmax=0.025;
				columnaxis / griddisplay=off gridattrs=(pattern=1) display=(label ticks tickvalues) labelattrs=(weight=bold) label="TT" type=linear offsetmin=0.025 offsetmax=0.025;
			endcolumnaxes;

			histogram em_gd / scale=percent fillattrs=(color=cxdddddd) endlabels=true;
			histogram em_tt / scale=percent fillattrs=(color=cxdddddd) endlabels=true;
		endlayout;
	endgraph;
end;
run;
ods html style=journal gpath="&DATA_DIR\out\"; ods graphics / /*reset*/ scale=off width=10in height=2.5in imagename="hist-dur" imagefmt=png border=off;
proc sgrender data=D_roi_ps1_a1_80 (where=(em_wlen_norm > 0 and em_wlen_norm <= 11)) template=HistDur; run;
quit;


/*
proc sort data=D_roi_ps1_a1_80; by sub m; run;
data tmp;
	set D_roi_ps1_a1_80;
	if (em_afd <> .);
	em_afd_log = log(em_afd);
	em_sfd_log = log(em_sfd);
	em_ffd_log = log(em_ffd);
	em_gd_log  = log(em_gd);
	em_tt_log  = log(em_tt);
	keep m se em_wlen_norm em_afdem_afd _log em_sfd em_sfd_log em_ffd em_ffd_log em_gd em_gd_log em_tt em_tt_log;
run;
*/

goptions reset=all hsize=20in vsize=10in ftext=swiss noborder lfactor=1 device=png gsfname=graphout gsfmode=replace;
filename graphout "&DATA_DIR\out\hist-fd.png";
proc capability data=D_roi_ps1_a1_80 (where=(em_wlen_norm > 0 and em_wlen_norm <= 11)) graphics noprint;
	axis1 label=(h=2 '%') offset=(0 2) value=(h=2) w=4;
	axis2 label=(h=2 'GD [ms]') offset=(2) value=(h=2) w=4;
	legend1 label=none cborder=black position=(bottom right inside) offset=(-2 1);

	comphistogram em_gd / vaxis=axis1 haxis=axis2 grid height=3 vscale=percent cfill=white cprop=gray intertile=1 lhref=2;
	inset n min max mean (3.0) median mode std (3.0) / position=ne height=4 cfill=white;
run;
quit;



* Histogram: em_wlen_norm;
/*
proc template;
	define statgraph Hist;
	dynamic _xvar _xlabel _binstart _binwidth _nbins;
	begingraph;
		layout overlay;
			histogram _xvar / binstart=_binstart binwidth=_binwidth nbins=_nbins scale=count fillattrs=(color=cxdddddd) endlabels=true;
		endlayout;
	endgraph;
end;
run;
ods html style=journal gpath="&DATA_DIR\out\"; ods graphics / reset width=10in height=5in imagename="hist-em_wlen_norm" imagefmt=png border=off;
proc sgrender data=D_roi_ps1_a1_80 (where=(em_wlen_norm > 0 and em_wlen_norm <= 13)) template=Hist; dynamic _xvar="em_wlen_norm" _xlabel="word length" _binstart=1 _binwidth=1 _nbins=16; run;
*/



proc freq data=D_roi_ps1_a1_80 (where=(em_wlen_norm > 0)); tables em_wlen_norm; run;

proc sort data=D_roi_ps1_a1_80; by sub m; run;
goptions reset=all hsize=20in vsize=10in ftext=swiss noborder lfactor=1 device=png gsfname=graphout gsfmode=replace;
filename graphout "&DATA_DIR\out\hist-em_wlen_norm.png";
proc capability data=D_roi_ps1_a1_80 (where=(em_wlen_norm > 0 and em_wlen_norm <= 16)) graphics noprint;
	axis1 label=(h=2 'count') offset=(0 2) value=(h=2) w=4;
	axis2 label=(h=2 'word length [character spaces]') offset=(2) value=(h=2) w=4;
	legend1 label=none cborder=black position=(bottom right inside) offset=(-2 1);

	comphistogram em_wlen_norm / vaxis=axis1 haxis=axis2 grid height=3  barlabel=percent vscale=count cfill=white cprop=gray intertile=1 lhref=2 midpoints=1 to 16 by 1;
	inset n min max mean (3.0) median mode std (3.0) / position=ne height=4 cfill=white;
run;
quit;


* Histogram: mrc_kffrq_log10_disc;
proc freq data=D_roi_ps1_a1_80 (where=(em_wlen_norm > 0 and em_wlen_norm <= 13)); tables mrc_kffrq_log10_disc; run;

proc sort data=D_roi_ps1_a1_80; by sub m; run;
goptions reset=all hsize=10in vsize=10in ftext=swiss noborder lfactor=1 device=png gsfname=graphout gsfmode=replace;
filename graphout "&DATA_DIR\out\hist-mrc_kffrq_log10_disc.png";
proc capability data=D_roi_ps1_a1_80 (where=(em_wlen_norm > 0 and em_wlen_norm <= 13)) graphics noprint;
	axis1 label=(h=1 'count') offset=(0 2) value=(h=1) w=2;
	axis2 label=(h=1 'word (log) frequency') offset=(2) value=(h=1) w=2;
	legend1 label=none cborder=black position=(bottom right inside) offset=(-2 1);

	comphistogram mrc_kffrq_log10_disc / vaxis=axis1 haxis=axis2 grid height=1.5 barlabel=percent vscale=count cfill=white cprop=gray intertile=1 lhref=2 midpoints=1 to 6 by 1;
	inset n min max mean (3.0) median mode std (3.0) / position=ne height=2 cfill=white;
run;
quit;


* Histogram: mrc_aoa_disc;
proc freq data=D_roi_ps1_a1_80 (where=(em_wlen_norm > 0 and em_wlen_norm <= 13)); tables mrc_aoa_disc; run;

proc sort data=D_roi_ps1_a1_80; by sub m; run;
goptions reset=all hsize=10in vsize=5in ftext=swiss noborder lfactor=1 device=png gsfname=graphout gsfmode=replace;
filename graphout "&DATA_DIR\out\hist-mrc_aoa_disc.png";
proc capability data=D_roi_ps1_a1_80 (where=(em_wlen_norm > 0 and em_wlen_norm <= 13)) graphics noprint;
	axis1 label=(h=1 'count') offset=(0 2) value=(h=1) w=2;
	axis2 label=(h=1 'word age of acquisition') offset=(2) value=(h=1) w=2;
	legend1 label=none cborder=black position=(bottom right inside) offset=(-2 1);

	comphistogram mrc_aoa_disc / vaxis=axis1 haxis=axis2 grid height=1.5 barlabel=percent vscale=count cfill=white cprop=gray intertile=1 lhref=2 midpoints=1 to 6 by 1;
	inset n min max mean (3.0) median mode std (3.0) / position=ne height=2 cfill=white;
run;
quit;


* Histogram: mrc_fam_disc;
proc freq data=D_roi_ps1_a1_80 (where=(em_wlen_norm > 0 and em_wlen_norm <= 13)); tables mrc_fam_disc; run;

proc sort data=D_roi_ps1_a1_80; by sub m; run;
goptions reset=all hsize=10in vsize=5in ftext=swiss noborder lfactor=1 device=png gsfname=graphout gsfmode=replace;
filename graphout "&DATA_DIR\out\hist-mrc_fam_disc.png";
proc capability data=D_roi_ps1_a1_80 graphics noprint;
	where em_wlen_norm > 0;
	axis1 label=(h=1 'count') offset=(0 2) value=(h=1) w=2;
	axis2 label=(h=1 'word familiarity') offset=(2) value=(h=1) w=2;
	legend1 label=none cborder=black position=(bottom right inside) offset=(-2 1);

	comphistogram mrc_fam_disc / vaxis=axis1 haxis=axis2 grid height=1.5 barlabel=percent vscale=count cfill=white cprop=gray intertile=1 lhref=2 midpoints=1 to 6 by 1;
	inset n min max mean (3.0) median mode std (3.0) / position=ne height=2 cfill=white;
run;
quit;




* --- ANALYSIS ---;


* Prior effects: Word length and frequency;
/*
proc sort data=D_roi_ps1_a1_80; by sub em_wlen_norm mrc_kffrq_log10_disc; run;
proc glimmix data=D_roi_ps1_a1_80 (where=(em_wlen_norm > 0 and em_wlen_norm <= 11)) order=data;
	class sub em_wlen_norm mrc_kffrq_log10_disc;
	model em_nfpf = em_wlen_norm mrc_kffrq_log10_disc / d=nb link=log;
	random int / sub=sub gcorr;
	random _residual_;

	estimate
		"len-1"    intercept 1   em_wlen_norm 1 0 0 0 0 0 0 0 0 0 0,
		"len-2"    intercept 1   em_wlen_norm 0 1 0 0 0 0 0 0 0 0 0,
		"len-3"    intercept 1   em_wlen_norm 0 0 1 0 0 0 0 0 0 0 0,
		"len-4"    intercept 1   em_wlen_norm 0 0 0 1 0 0 0 0 0 0 0,
		"len-5"    intercept 1   em_wlen_norm 0 0 0 0 1 0 0 0 0 0 0,
		"len-6"    intercept 1   em_wlen_norm 0 0 0 0 0 1 0 0 0 0 0,
		"len-7"    intercept 1   em_wlen_norm 0 0 0 0 0 0 1 0 0 0 0,
		"len-8"    intercept 1   em_wlen_norm 0 0 0 0 0 0 0 1 0 0 0,
		"len-9"    intercept 1   em_wlen_norm 0 0 0 0 0 0 0 0 1 0 0,
		"len-10"   intercept 1   em_wlen_norm 0 0 0 0 0 0 0 0 0 1 0,
		"len-11"   intercept 1   em_wlen_norm 0 0 0 0 0 0 0 0 0 0 1,

		"frq-1"    intercept 1   mrc_kffrq_log10_disc 1 0 0 0 0 0,
		"frq-2"    intercept 1   mrc_kffrq_log10_disc 0 1 0 0 0 0,
		"frq-3"    intercept 1   mrc_kffrq_log10_disc 0 0 1 0 0 0,
		"frq-4"    intercept 1   mrc_kffrq_log10_disc 0 0 0 1 0 0,
		"frq-5"    intercept 1   mrc_kffrq_log10_disc 0 0 0 0 1 0,
		"frq-6"    intercept 1   mrc_kffrq_log10_disc 0 0 0 0 0 1 / ilink cl;

	output out=tmp pearson(blup)=res;
run;
proc means data=tmp mean std; var res; run;
*/


proc sort data=D_roi_ps1_a1_80; by sub em_wlen_norm mrc_kffrq_log10_disc; run;
proc glimmix data=D_roi_ps1_a1_80 (where=(em_wlen_norm > 0 and em_wlen_norm <= 11)) order=data;
	class sub em_nfpf_nz em_wlen_norm mrc_kffrq_log10_disc;
	model em_nfpf_nz (event='1') = em_wlen_norm mrc_kffrq_log10_disc / d=binary link=logit;
	random int / sub=sub gcorr;
	random _residual_;

	estimate
		"len-1"    intercept 1   em_wlen_norm 1 0 0 0 0 0 0 0 0 0 0,
		"len-2"    intercept 1   em_wlen_norm 0 1 0 0 0 0 0 0 0 0 0,
		"len-3"    intercept 1   em_wlen_norm 0 0 1 0 0 0 0 0 0 0 0,
		"len-4"    intercept 1   em_wlen_norm 0 0 0 1 0 0 0 0 0 0 0,
		"len-5"    intercept 1   em_wlen_norm 0 0 0 0 1 0 0 0 0 0 0,
		"len-6"    intercept 1   em_wlen_norm 0 0 0 0 0 1 0 0 0 0 0,
		"len-7"    intercept 1   em_wlen_norm 0 0 0 0 0 0 1 0 0 0 0,
		"len-8"    intercept 1   em_wlen_norm 0 0 0 0 0 0 0 1 0 0 0,
		"len-9"    intercept 1   em_wlen_norm 0 0 0 0 0 0 0 0 1 0 0,
		"len-10"   intercept 1   em_wlen_norm 0 0 0 0 0 0 0 0 0 1 0,
		"len-11"   intercept 1   em_wlen_norm 0 0 0 0 0 0 0 0 0 0 1,

		"frq-1"    intercept 1   mrc_kffrq_log10_disc 1 0 0 0 0 0,
		"frq-2"    intercept 1   mrc_kffrq_log10_disc 0 1 0 0 0 0,
		"frq-3"    intercept 1   mrc_kffrq_log10_disc 0 0 1 0 0 0,
		"frq-4"    intercept 1   mrc_kffrq_log10_disc 0 0 0 1 0 0,
		"frq-5"    intercept 1   mrc_kffrq_log10_disc 0 0 0 0 1 0,
		"frq-6"    intercept 1   mrc_kffrq_log10_disc 0 0 0 0 0 1 / ilink cl;

	output out=tmp pearson(blup)=res;
run;
proc means data=tmp mean std; var res; run;


proc sort data=D_roi_ps1_a1_80; by em_wlen_norm mrc_kffrq_log10_disc sub; run;
proc glimmix data=D_roi_ps1_a1_80 (where=(em_wlen_norm > 0 and em_wlen_norm <= 11)) order=data;
	class sub em_wlen_norm mrc_kffrq_log10_disc;
	model em_sfd = em_wlen_norm mrc_kffrq_log10_disc / d=logn link=identity;
	random int / sub=sub gcorr;

	estimate
		"len-1"    intercept 1   em_wlen_norm 1 0 0 0 0 0 0 0 0 0 0,
		"len-2"    intercept 1   em_wlen_norm 0 1 0 0 0 0 0 0 0 0 0,
		"len-3"    intercept 1   em_wlen_norm 0 0 1 0 0 0 0 0 0 0 0,
		"len-4"    intercept 1   em_wlen_norm 0 0 0 1 0 0 0 0 0 0 0,
		"len-5"    intercept 1   em_wlen_norm 0 0 0 0 1 0 0 0 0 0 0,
		"len-6"    intercept 1   em_wlen_norm 0 0 0 0 0 1 0 0 0 0 0,
		"len-7"    intercept 1   em_wlen_norm 0 0 0 0 0 0 1 0 0 0 0,
		"len-8"    intercept 1   em_wlen_norm 0 0 0 0 0 0 0 1 0 0 0,
		"len-9"    intercept 1   em_wlen_norm 0 0 0 0 0 0 0 0 1 0 0,
		"len-10"   intercept 1   em_wlen_norm 0 0 0 0 0 0 0 0 0 1 0,
		"len-11"   intercept 1   em_wlen_norm 0 0 0 0 0 0 0 0 0 0 1,

		"frq-1"    intercept 1   mrc_kffrq_log10_disc 1 0 0 0 0 0,
		"frq-2"    intercept 1   mrc_kffrq_log10_disc 0 1 0 0 0 0,
		"frq-3"    intercept 1   mrc_kffrq_log10_disc 0 0 1 0 0 0,
		"frq-4"    intercept 1   mrc_kffrq_log10_disc 0 0 0 1 0 0,
		"frq-5"    intercept 1   mrc_kffrq_log10_disc 0 0 0 0 1 0,
		"frq-6"    intercept 1   mrc_kffrq_log10_disc 0 0 0 0 0 1 / exp cl;
	output out=tmp pearson(blup)=res;
run;
proc means data=tmp mean std; var res; run;




* Prior effects: Word age of acquisition;
/*
proc sort data=D_roi_ps1_a1_80; by mrc_aoa_disc sub; run;
proc glimmix data=D_roi_ps1_a1_80 (where=(em_wlen_norm > 0 and em_wlen_norm <= 11)) order=data;
	class sub mrc_aoa_disc mrc_kffrq_log10_disc;
	model em_nf = mrc_aoa_disc mrc_kffrq_log10_disc / d=p link=log;
	random int / sub=sub gcorr;
	random _residual_;

	estimate
		"aoa-1"   intercept 1   mrc_aoa_disc 1 0 0 0 0 0,
		"aoa-2"   intercept 1   mrc_aoa_disc 0 1 0 0 0 0,
		"aoa-3"   intercept 1   mrc_aoa_disc 0 0 1 0 0 0,
		"aoa-4"   intercept 1   mrc_aoa_disc 0 0 0 1 0 0,
		"aoa-5"   intercept 1   mrc_aoa_disc 0 0 0 0 1 0,
		"aoa-6"   intercept 1   mrc_aoa_disc 0 0 0 0 0 1 / ilink cl;

	output out=tmp pearson(blup)=res;
run;
proc means data=tmp mean std; var res; run;
*/


proc sort data=D_roi_ps1_a1_80; by mrc_aoa_disc sub; run;
proc glimmix data=D_roi_ps1_a1_80 (where=(em_wlen_norm > 0 and em_wlen_norm <= 11)) order=data;
	class sub em_nfpf_nz mrc_aoa_disc mrc_kffrq_log10_disc;
	model em_nfpf_nz (event='1') = mrc_aoa_disc mrc_kffrq_log10_disc / d=binary link=logit;
	random int / sub=sub gcorr;
	random _residual_;

	estimate
		"aoa-1"   intercept 1   mrc_aoa_disc 1 0 0 0 0 0,
		"aoa-2"   intercept 1   mrc_aoa_disc 0 1 0 0 0 0,
		"aoa-3"   intercept 1   mrc_aoa_disc 0 0 1 0 0 0,
		"aoa-4"   intercept 1   mrc_aoa_disc 0 0 0 1 0 0,
		"aoa-5"   intercept 1   mrc_aoa_disc 0 0 0 0 1 0,
		"aoa-6"   intercept 1   mrc_aoa_disc 0 0 0 0 0 1 / ilink cl;

	output out=tmp pearson(blup)=res;
run;
proc means data=tmp mean std; var res; run;


proc sort data=D_roi_ps1_a1_80; by mrc_aoa_disc sub; run;
proc glimmix data=D_roi_ps1_a1_80 (where=(em_wlen_norm > 0 and em_wlen_norm <= 11)) order=data;
	class sub mrc_aoa_disc mrc_kffrq_log10_disc;
	model em_sfd = mrc_aoa_disc mrc_kffrq_log10_disc / d=logn link=identity;
	random int / sub=sub gcorr;

	estimate
		"aoa-1"   intercept 1   mrc_aoa_disc 1 0 0 0 0 0,
		"aoa-2"   intercept 1   mrc_aoa_disc 0 1 0 0 0 0,
		"aoa-3"   intercept 1   mrc_aoa_disc 0 0 1 0 0 0,
		"aoa-4"   intercept 1   mrc_aoa_disc 0 0 0 1 0 0,
		"aoa-5"   intercept 1   mrc_aoa_disc 0 0 0 0 1 0,
		"aoa-6"   intercept 1   mrc_aoa_disc 0 0 0 0 0 1 / exp cl;

	output out=tmp pearson(blup)=res;
run;
proc means data=tmp mean std; var res; run;




* Prior effect: Word familiarity;
proc sort data=D_roi_ps1_a1_80; by mrc_fam_disc sub; run;
proc glimmix data=D_roi_ps1_a1_80 (where=(em_wlen_norm > 0 and em_wlen_norm <= 11)) order=data;
	class sub mrc_fam_disc mrc_kffrq_log10_disc;
	model em_nf = mrc_fam_disc mrc_kffrq_log10_disc / d=nb link=log;
	random int / sub=sub gcorr;
	*random _residual_;

	estimate
		"fam-2"   intercept 1   mrc_fam_disc 1 0 0 0 0,
		"fam-3"   intercept 1   mrc_fam_disc 0 1 0 0 0,
		"fam-4"   intercept 1   mrc_fam_disc 0 0 1 0 0,
		"fam-5"   intercept 1   mrc_fam_disc 0 0 0 1 0,
		"fam-6"   intercept 1   mrc_fam_disc 0 0 0 0 1 / ilink cl;

	output out=tmp pearson(blup)=res;
run;
proc means data=tmp mean std; var res; run;


proc sort data=D_roi_ps1_a1_80; by mrc_fam_disc sub; run;
proc glimmix data=D_roi_ps1_a1_80 (where=(em_wlen_norm > 0 and em_wlen_norm <= 11)) order=data maxopt=500;
	nloptions miniter=500;
	class sub em_nf_nz mrc_fam_disc mrc_kffrq_log10_disc;
	model em_nf_nz (event='1') = mrc_fam_disc mrc_kffrq_log10_disc / d=binary link=logit;
	random int / sub=sub gcorr;
	random _residual_;

	estimate
		"fam-2"   intercept 1   mrc_fam_disc 1 0 0 0 0,
		"fam-3"   intercept 1   mrc_fam_disc 0 1 0 0 0,
		"fam-4"   intercept 1   mrc_fam_disc 0 0 1 0 0,
		"fam-5"   intercept 1   mrc_fam_disc 0 0 0 1 0,
		"fam-6"   intercept 1   mrc_fam_disc 0 0 0 0 1 / ilink cl;

	output out=tmp pearson(blup)=res;
run;
proc means data=tmp mean std; var res; run;


proc sort data=D_roi_ps1_a1_80; by mrc_fam_disc sub; run;
proc glimmix data=D_roi_ps1_a1_80 (where=(em_wlen_norm > 0 and em_wlen_norm <= 11)) order=data;
	class sub mrc_fam_disc mrc_kffrq_log10_disc;
	model em_sfd = mrc_fam_disc mrc_kffrq_log10_disc / d=logn link=identity;
	random int / sub=sub gcorr;

	estimate
		"fam-2"   intercept 1   mrc_fam_disc 1 0 0 0 0,
		"fam-3"   intercept 1   mrc_fam_disc 0 1 0 0 0,
		"fam-4"   intercept 1   mrc_fam_disc 0 0 1 0 0,
		"fam-5"   intercept 1   mrc_fam_disc 0 0 0 1 0,
		"fam-6"   intercept 1   mrc_fam_disc 0 0 0 0 1 / exp cl;

	output out=tmp pearson(blup)=res;
run;
proc means data=tmp mean std; var res; run;




* Prior effects: Plots;
proc template;
	define statgraph PriorEffects;
	begingraph;
		layout lattice / columns=4 rows=7 columngutter=0 rowgutter=0 skipemptycells=true rowdatarange=union columndatarange=union;
			rowaxes;
				rowaxis / griddisplay=on gridattrs=(pattern=1) display=(label ticks tickvalues) label="%"    labelattrs=(weight=bold) offsetmin=0.1;
				rowaxis / griddisplay=on gridattrs=(pattern=1) display=(label ticks tickvalues) label="NF"   labelattrs=(weight=bold) offsetmin=0.1;
				rowaxis / griddisplay=on gridattrs=(pattern=1) display=(label ticks tickvalues) label="NFPF" labelattrs=(weight=bold) offsetmin=0.1;
				rowaxis / griddisplay=on gridattrs=(pattern=1) display=(label ticks tickvalues) label="SFD"  labelattrs=(weight=bold) offsetmin=0.1;
				rowaxis / griddisplay=on gridattrs=(pattern=1) display=(label ticks tickvalues) label="FFD"  labelattrs=(weight=bold) offsetmin=0.1;
				rowaxis / griddisplay=on gridattrs=(pattern=1) display=(label ticks tickvalues) label="GD"   labelattrs=(weight=bold) offsetmin=0.1;
				rowaxis / griddisplay=on gridattrs=(pattern=1) display=(label ticks tickvalues) label="TT"   labelattrs=(weight=bold) offsetmin=0.1;
			endrowaxes;

			columnaxes;
				columnaxis / griddisplay=on gridattrs=(pattern=1) display=(label ticks tickvalues) labelattrs=(weight=bold) label="LEN" type=linear linearopts=(viewmin=1 viewmax=11);
				columnaxis / griddisplay=on gridattrs=(pattern=1) display=(label ticks tickvalues) labelattrs=(weight=bold) label="FRQ" type=linear linearopts=(viewmin=1 viewmax=6);
				columnaxis / griddisplay=on gridattrs=(pattern=1) display=(label ticks tickvalues) labelattrs=(weight=bold) label="AOA" type=linear linearopts=(viewmin=1 viewmax=6);
				columnaxis / griddisplay=on gridattrs=(pattern=1) display=(label ticks tickvalues) labelattrs=(weight=bold) label="FAM" type=linear linearopts=(viewmin=1 viewmax=6);
			endcolumnaxes;

			layout overlay / pad=(bottom=0.05in);  histogram x / freq=len_cnt binstart=1 binwidth=1 nbins=11 scale=percent fillattrs=(color=cxdddddd);  entry "N=20613 (100%)" / autoalign=(topright);  endlayout;
			layout overlay / pad=(bottom=0.05in);  histogram x / freq=frq_cnt binstart=1 binwidth=1 nbins=6  scale=percent fillattrs=(color=cxdddddd);  entry "N=14378 (70%)"  / autoalign=(topright);  endlayout;
			layout overlay / pad=(bottom=0.05in);  histogram x / freq=aoa_cnt binstart=1 binwidth=1 nbins=6  scale=percent fillattrs=(color=cxdddddd);  entry "N=1351 (7%)"    / autoalign=(topright);  endlayout;
			layout overlay / pad=(bottom=0.05in);  histogram x / freq=fam_cnt binstart=1 binwidth=1 nbins=6  scale=percent fillattrs=(color=cxdddddd);  entry "N=11535 (56%)"  / autoalign=(topleft);   endlayout;

			layout overlay;  bandplot x=x limitlower=len_nf_l95   limitupper=len_nf_u95   / display=(fill outline) fillattrs=(color=cxdddddd) outlineattrs=(thickness=1 color=cx999999);  seriesplot y=len_nf_m x=x   / lineattrs=(thickness=2);  /*entry "p=." / autoalign=(topleft); */  endlayout;
			layout overlay;  bandplot x=x limitlower=frq_nf_l95   limitupper=frq_nf_u95   / display=(fill outline) fillattrs=(color=cxdddddd) outlineattrs=(thickness=1 color=cx999999);  seriesplot y=frq_nf_m x=x   / lineattrs=(thickness=2);  /*entry "p=." / autoalign=(topleft); */  endlayout;
			layout overlay;  bandplot x=x limitlower=aoa_nf_l95   limitupper=aoa_nf_u95   / display=(fill outline) fillattrs=(color=cxdddddd) outlineattrs=(thickness=1 color=cx999999);  seriesplot y=aoa_nf_m x=x   / lineattrs=(thickness=2);  /*entry "p=." / autoalign=(topleft); */  endlayout;
			layout overlay;  bandplot x=x limitlower=fam_nf_l95   limitupper=fam_nf_u95   / display=(fill outline) fillattrs=(color=cxdddddd) outlineattrs=(thickness=1 color=cx999999);  seriesplot y=fam_nf_m x=x   / lineattrs=(thickness=2);  /*entry "p=." / autoalign=(topright);*/  endlayout;

			layout overlay;  bandplot x=x limitlower=len_nfpf_l95 limitupper=len_nfpf_u95 / display=(fill outline) fillattrs=(color=cxdddddd) outlineattrs=(thickness=1 color=cx999999);  seriesplot y=len_nfpf_m x=x / lineattrs=(thickness=2);  /*entry "p=." / autoalign=(topleft); */  endlayout;
			layout overlay;  bandplot x=x limitlower=frq_nfpf_l95 limitupper=frq_nfpf_u95 / display=(fill outline) fillattrs=(color=cxdddddd) outlineattrs=(thickness=1 color=cx999999);  seriesplot y=frq_nfpf_m x=x / lineattrs=(thickness=2);  /*entry "p=." / autoalign=(topleft); */  endlayout;
			layout overlay;  bandplot x=x limitlower=aoa_nfpf_l95 limitupper=aoa_nfpf_u95 / display=(fill outline) fillattrs=(color=cxdddddd) outlineattrs=(thickness=1 color=cx999999);  seriesplot y=aoa_nfpf_m x=x / lineattrs=(thickness=2);  /*entry "p=." / autoalign=(topleft); */  endlayout;
			layout overlay;  bandplot x=x limitlower=fam_nfpf_l95 limitupper=fam_nfpf_u95 / display=(fill outline) fillattrs=(color=cxdddddd) outlineattrs=(thickness=1 color=cx999999);  seriesplot y=fam_nfpf_m x=x / lineattrs=(thickness=2);  /*entry "p=." / autoalign=(topright);*/  endlayout;

			layout overlay;  bandplot x=x limitlower=len_sfd_l95  limitupper=len_sfd_u95  / display=(fill outline) fillattrs=(color=cxdddddd) outlineattrs=(thickness=1 color=cx999999);  seriesplot y=len_sfd_m x=x  / lineattrs=(thickness=2);  /*entry "p=." / autoalign=(topleft); */  endlayout;
			layout overlay;  bandplot x=x limitlower=frq_sfd_l95  limitupper=frq_sfd_u95  / display=(fill outline) fillattrs=(color=cxdddddd) outlineattrs=(thickness=1 color=cx999999);  seriesplot y=frq_sfd_m x=x  / lineattrs=(thickness=2);  /*entry "p=." / autoalign=(topleft); */  endlayout;
			layout overlay;  bandplot x=x limitlower=aoa_sfd_l95  limitupper=aoa_sfd_u95  / display=(fill outline) fillattrs=(color=cxdddddd) outlineattrs=(thickness=1 color=cx999999);  seriesplot y=aoa_sfd_m x=x  / lineattrs=(thickness=2);  /*entry "p=." / autoalign=(topleft); */  endlayout;
			layout overlay;  bandplot x=x limitlower=fam_sfd_l95  limitupper=fam_sfd_u95  / display=(fill outline) fillattrs=(color=cxdddddd) outlineattrs=(thickness=1 color=cx999999);  seriesplot y=fam_sfd_m x=x  / lineattrs=(thickness=2);  /*entry "p=." / autoalign=(topright);*/  endlayout;

			layout overlay;  bandplot x=x limitlower=len_ffd_l95  limitupper=len_ffd_u95  / display=(fill outline) fillattrs=(color=cxdddddd) outlineattrs=(thickness=1 color=cx999999);  seriesplot y=len_ffd_m x=x  / lineattrs=(thickness=2);  /*entry "p=." / autoalign=(topleft); */  endlayout;
			layout overlay;  bandplot x=x limitlower=frq_ffd_l95  limitupper=frq_ffd_u95  / display=(fill outline) fillattrs=(color=cxdddddd) outlineattrs=(thickness=1 color=cx999999);  seriesplot y=frq_ffd_m x=x  / lineattrs=(thickness=2);  /*entry "p=." / autoalign=(topleft); */  endlayout;
			layout overlay;  bandplot x=x limitlower=aoa_ffd_l95  limitupper=aoa_ffd_u95  / display=(fill outline) fillattrs=(color=cxdddddd) outlineattrs=(thickness=1 color=cx999999);  seriesplot y=aoa_ffd_m x=x  / lineattrs=(thickness=2);  /*entry "p=." / autoalign=(topleft); */  endlayout;
			layout overlay;  bandplot x=x limitlower=fam_ffd_l95  limitupper=fam_ffd_u95  / display=(fill outline) fillattrs=(color=cxdddddd) outlineattrs=(thickness=1 color=cx999999);  seriesplot y=fam_ffd_m x=x  / lineattrs=(thickness=2);  /*entry "p=." / autoalign=(topright);*/  endlayout;

			layout overlay;  bandplot x=x limitlower=len_gd_l95   limitupper=len_gd_u95   / display=(fill outline) fillattrs=(color=cxdddddd) outlineattrs=(thickness=1 color=cx999999);  seriesplot y=len_gd_m x=x   / lineattrs=(thickness=2);  /*entry "p=." / autoalign=(topleft); */  endlayout;
			layout overlay;  bandplot x=x limitlower=frq_gd_l95   limitupper=frq_gd_u95   / display=(fill outline) fillattrs=(color=cxdddddd) outlineattrs=(thickness=1 color=cx999999);  seriesplot y=frq_gd_m x=x   / lineattrs=(thickness=2);  /*entry "p=." / autoalign=(topleft); */  endlayout;
			layout overlay;  bandplot x=x limitlower=aoa_gd_l95   limitupper=aoa_gd_u95   / display=(fill outline) fillattrs=(color=cxdddddd) outlineattrs=(thickness=1 color=cx999999);  seriesplot y=aoa_gd_m x=x   / lineattrs=(thickness=2);  /*entry "p=." / autoalign=(topleft); */  endlayout;
			layout overlay;  bandplot x=x limitlower=fam_gd_l95   limitupper=fam_gd_u95   / display=(fill outline) fillattrs=(color=cxdddddd) outlineattrs=(thickness=1 color=cx999999);  seriesplot y=fam_gd_m x=x   / lineattrs=(thickness=2);  /*entry "p=." / autoalign=(topright);*/  endlayout;

			layout overlay;  bandplot x=x limitlower=len_tt_l95   limitupper=len_tt_u95   / display=(fill outline) fillattrs=(color=cxdddddd) outlineattrs=(thickness=1 color=cx999999);  seriesplot y=len_tt_m x=x   / lineattrs=(thickness=2);  /*entry "p=." / autoalign=(topleft); */  endlayout;
			layout overlay;  bandplot x=x limitlower=frq_tt_l95   limitupper=frq_tt_u95   / display=(fill outline) fillattrs=(color=cxdddddd) outlineattrs=(thickness=1 color=cx999999);  seriesplot y=frq_tt_m x=x   / lineattrs=(thickness=2);  /*entry "p=." / autoalign=(topleft); */  endlayout;
			layout overlay;  bandplot x=x limitlower=aoa_tt_l95   limitupper=aoa_tt_u95   / display=(fill outline) fillattrs=(color=cxdddddd) outlineattrs=(thickness=1 color=cx999999);  seriesplot y=aoa_tt_m x=x   / lineattrs=(thickness=2);  /*entry "p=." / autoalign=(topleft); */  endlayout;
			layout overlay;  bandplot x=x limitlower=fam_tt_l95   limitupper=fam_tt_u95   / display=(fill outline) fillattrs=(color=cxdddddd) outlineattrs=(thickness=1 color=cx999999);  seriesplot y=fam_tt_m x=x   / lineattrs=(thickness=2);  /*entry "p=." / autoalign=(topright);*/  endlayout;
		endlayout;
	endgraph;
end;
run;
ods html style=journal gpath="&DATA_DIR\out\"; ods graphics / /* reset*/ width=10in height=10in /*ftext=swiss lfactor=1*/ imagename="prior-effects" imagefmt=png border=off;
proc sgrender data=D_prior_eff_ps1_a1_80 template=PriorEffects; run;




* Current effects: Contrasts;
/*

- m se=0:
            se
         |  0  1 |
         |  1  0 |
    -----+-------+---
  m 0  1 |  1  0 |  1
    1 -1 | -1  0 | -1
    -----+-------+---
         |  0  0 |  0

  estimate 'm se0'   m  1 -1   m*se  1  0 -1  0;

- m se=1:
            se
         |  0  1 |
         |  0  1 |
    -----+-------+---
  m 0  1 |  0  1 |  1
    1 -1 |  0 -1 | -1
    -----+-------+---
         |  0  0 |  0

  estimate 'm se1'   m  1 -1   m*se  0  1  0 -1;

*/




* Current effects: Counts;
proc sort data=D_roi_ps1_a1_80; by m_wlen_norm mrc_kffrq_log10_disc sub m se; run;
proc glimmix data=D_roi_ps1_a1_80 (where=(em_wlen_norm > 0 and em_wlen_norm <= 11)) order=data;
	*where em_nf > 0;
	class sub m se em_wlen_norm mrc_kffrq_log10_disc;
	model em_nr = m | se em_wlen_norm mrc_kffrq_log10_disc / d=nb link=log;
	random int / sub=sub gcorr;
	random _residual_;* / grp=m;
	lsmeans m m*se / ilink cl;

    estimate 'm se0'   m  1 -1   m*se  1  0 -1  0;
	estimate 'm se1'   m  1 -1   m*se  0  1  0 -1;

	output out=tmp pearson(blup)=res;
run;
proc means data=tmp mean std; var res; run;




* Current effects: Probabilities;
proc sort data=D_roi_ps1_a1_80; by em_wlen_norm mrc_kffrq_log10_disc sub m se; run;
proc glimmix data=D_roi_ps1_a1_80 (where=(em_wlen_norm > 0 and em_wlen_norm <= 11)) order=data;
	class em_nf_0 sub m se em_wlen_norm mrc_kffrq_log10_disc;
	*x = n-em_nf_nz;
	model em_nf_0 = m | se em_wlen_norm mrc_kffrq_log10_disc / d=binary link=logit or;
	random int / sub=sub gcorr;
	random _residual_;* / grp=m;
	lsmeans m m*se / ilink cl or;

    estimate 'm se0'   m  1 -1   m*se  1  0 -1  0;
	estimate 'm se1'   m  1 -1   m*se  0  1  0 -1;

	output out=tmp pearson(blup)=res;
run;
proc means data=tmp mean std; var res; run;


proc sort data=D_roi_ps1_a1_80; by em_wlen_norm mrc_kffrq_log10_disc sub m se; run;
proc nlmixed data=D_roi_ps1_a1_80 qpoints=20 tech=newrap;
	parms beta0=-1 beta1=1 beta2=1 beta3=1 beta4=1 beta5=1 s2u=2;
	eta = beta0 + beta1*m + beta2*se + beta3*m*se + beta4*em_wlen_norm + beta5*mrc_kffrq_log10_disc + u;
	mu = exp(eta) / (1 + exp(eta));
	model em_nf_1 ~ binary(mu);
	random u ~ normal(0,s2u) subject=sub;
	predict eta out=eta;
	estimate '1/beta1' 1/beta1;
	estimate '1/beta2' 1/beta2;
	estimate '1/beta3' 1/beta3;
	estimate '1/beta4' 1/beta4;
	estimate '1/beta5' 1/beta5;
run;




* Current effects: Durations;
proc sort data=D_roi_ps1_a1_80; by em_wlen_norm mrc_kffrq_log10_disc sub m se; run;
proc glimmix data=D_roi_ps1_a1_80 (where=(em_nf > 0 and em_wlen_norm > 0 and em_wlen_norm <= 11)) order=data;
	class sub m se em_wlen_norm mrc_kffrq_log10_disc;
	model em_sfd = m | se em_wlen_norm mrc_kffrq_log10_disc / d=logn link=identity;
	random int / sub=sub gcorr;
	lsmeans m m*se / cl;

    estimate 'm se0'   m  1 -1   m*se  1  0 -1  0;
	estimate 'm se1'   m  1 -1   m*se  0  1  0 -1;

	output out=tmp pearson(blup)=res;
run;
proc means data=tmp mean std; var res; run;





* Classifier: Contrasts;
/*

- se=0 m=0:
            se
         |  0  1 |
         |  1  0 |
    -----+-------+---
  m 0  1 |  1  0 |  1
    1  0 |  0  0 |  0
    -----+-------+---
         |  0  1 |  1

  estimate 'm se1'   m  1 0   se 1 0   m*se 1 0 0 0;


- se=0 m=1:
            se
         |  0  1 |
         |  1  0 |
    -----+-------+---
  m 0  0 |  0  0 |  0
    1  1 |  1  0 |  1
    -----+-------+---
         |  0  1 |  1

  estimate 'm se1'   m  1 0   se 1 0   m*se 0 0 1 0;


- se=1 m=0:
            se
         |  0  1 |
         |  0  1 |
    -----+-------+---
  m 0  1 |  0  1 |  1
    1  0 |  0  0 |  0
    -----+-------+---
         |  0  1 |  1

  estimate 'm se1'   m  1 0   se 0 1   m*se 0 1 0 0;


- se=1 m=1:
            se
         |  0  1 |
         |  0  1 |
    -----+-------+---
  m 0  0 |  0  0 |  0
    1  1 |  0  1 |  1
    -----+-------+---
         |  0  1 |  1

  estimate 'm se1'   m  1 0   se 0 1   m*se 0 0 0 1;

*/




* Classifier: Estimates (len: SML, frq: 1..6);
proc sort data=D_roi_ps1_a1_80; by em_wlen_norm_sml mrc_kffrq_log10_disc sub m se; run;
proc glimmix data=D_roi_ps1_a1_80 (where=(em_nf > 0 and em_wlen_norm > 0 and em_wlen_norm <= 11)) order=data;
	class sub m se em_wlen_norm_sml mrc_kffrq_log10_disc;
	*model em_nf = m | se em_wlen_norm_sml mrc_kffrq_log10_disc / d=p link=log;
	model em_tt = m | se em_wlen_norm_sml mrc_kffrq_log10_disc / d=logn link=identity;
	random int / sub=sub gcorr;
	*random _residual_;

	* se m len frq;
	estimate
		'0 0 0 1'   intercept 1   se 1 0   em_wlen_norm_sml 1 0 0   m 1 0   m*se 1 0 0 0   mrc_kffrq_log10_disc 1 0 0 0 0 0,
		'0 0 0 2'   intercept 1   se 1 0   em_wlen_norm_sml 1 0 0   m 1 0   m*se 1 0 0 0   mrc_kffrq_log10_disc 0 1 0 0 0 0,
		'0 0 0 3'   intercept 1   se 1 0   em_wlen_norm_sml 1 0 0   m 1 0   m*se 1 0 0 0   mrc_kffrq_log10_disc 0 0 1 0 0 0,
		'0 0 0 4'   intercept 1   se 1 0   em_wlen_norm_sml 1 0 0   m 1 0   m*se 1 0 0 0   mrc_kffrq_log10_disc 0 0 0 1 0 0,
		'0 0 0 5'   intercept 1   se 1 0   em_wlen_norm_sml 1 0 0   m 1 0   m*se 1 0 0 0   mrc_kffrq_log10_disc 0 0 0 0 1 0,
		'0 0 0 6'   intercept 1   se 1 0   em_wlen_norm_sml 1 0 0   m 1 0   m*se 1 0 0 0   mrc_kffrq_log10_disc 0 0 0 0 0 1,
		'0 1 0 1'   intercept 1   se 1 0   em_wlen_norm_sml 1 0 0   m 0 1   m*se 0 0 1 0   mrc_kffrq_log10_disc 1 0 0 0 0 0,
		'0 1 0 2'   intercept 1   se 1 0   em_wlen_norm_sml 1 0 0   m 0 1   m*se 0 0 1 0   mrc_kffrq_log10_disc 0 1 0 0 0 0,
		'0 1 0 3'   intercept 1   se 1 0   em_wlen_norm_sml 1 0 0   m 0 1   m*se 0 0 1 0   mrc_kffrq_log10_disc 0 0 1 0 0 0,
		'0 1 0 4'   intercept 1   se 1 0   em_wlen_norm_sml 1 0 0   m 0 1   m*se 0 0 1 0   mrc_kffrq_log10_disc 0 0 0 1 0 0,
		'0 1 0 5'   intercept 1   se 1 0   em_wlen_norm_sml 1 0 0   m 0 1   m*se 0 0 1 0   mrc_kffrq_log10_disc 0 0 0 0 1 0,
		'0 1 0 6'   intercept 1   se 1 0   em_wlen_norm_sml 1 0 0   m 0 1   m*se 0 0 1 0   mrc_kffrq_log10_disc 0 0 0 0 0 1,

		'0 0 1 1'   intercept 1   se 1 0   em_wlen_norm_sml 0 1 0   m 1 0   m*se 1 0 0 0   mrc_kffrq_log10_disc 1 0 0 0 0 0,
		'0 0 1 2'   intercept 1   se 1 0   em_wlen_norm_sml 0 1 0   m 1 0   m*se 1 0 0 0   mrc_kffrq_log10_disc 0 1 0 0 0 0,
		'0 0 1 3'   intercept 1   se 1 0   em_wlen_norm_sml 0 1 0   m 1 0   m*se 1 0 0 0   mrc_kffrq_log10_disc 0 0 1 0 0 0,
		'0 0 1 4'   intercept 1   se 1 0   em_wlen_norm_sml 0 1 0   m 1 0   m*se 1 0 0 0   mrc_kffrq_log10_disc 0 0 0 1 0 0,
		'0 0 1 5'   intercept 1   se 1 0   em_wlen_norm_sml 0 1 0   m 1 0   m*se 1 0 0 0   mrc_kffrq_log10_disc 0 0 0 0 1 0,
		'0 0 1 6'   intercept 1   se 1 0   em_wlen_norm_sml 0 1 0   m 1 0   m*se 1 0 0 0   mrc_kffrq_log10_disc 0 0 0 0 0 1,
		'0 1 1 1'   intercept 1   se 1 0   em_wlen_norm_sml 0 1 0   m 0 1   m*se 0 0 1 0   mrc_kffrq_log10_disc 1 0 0 0 0 0,
		'0 1 1 2'   intercept 1   se 1 0   em_wlen_norm_sml 0 1 0   m 0 1   m*se 0 0 1 0   mrc_kffrq_log10_disc 0 1 0 0 0 0,
		'0 1 1 3'   intercept 1   se 1 0   em_wlen_norm_sml 0 1 0   m 0 1   m*se 0 0 1 0   mrc_kffrq_log10_disc 0 0 1 0 0 0,
		'0 1 1 4'   intercept 1   se 1 0   em_wlen_norm_sml 0 1 0   m 0 1   m*se 0 0 1 0   mrc_kffrq_log10_disc 0 0 0 1 0 0,
		'0 1 1 5'   intercept 1   se 1 0   em_wlen_norm_sml 0 1 0   m 0 1   m*se 0 0 1 0   mrc_kffrq_log10_disc 0 0 0 0 1 0,
		'0 1 1 6'   intercept 1   se 1 0   em_wlen_norm_sml 0 1 0   m 0 1   m*se 0 0 1 0   mrc_kffrq_log10_disc 0 0 0 0 0 1,

		'0 0 2 1'   intercept 1   se 1 0   em_wlen_norm_sml 0 0 1   m 1 0   m*se 1 0 0 0   mrc_kffrq_log10_disc 1 0 0 0 0 0,
		'0 0 2 2'   intercept 1   se 1 0   em_wlen_norm_sml 0 0 1   m 1 0   m*se 1 0 0 0   mrc_kffrq_log10_disc 0 1 0 0 0 0,
		'0 0 2 3'   intercept 1   se 1 0   em_wlen_norm_sml 0 0 1   m 1 0   m*se 1 0 0 0   mrc_kffrq_log10_disc 0 0 1 0 0 0,
		'0 0 2 4'   intercept 1   se 1 0   em_wlen_norm_sml 0 0 1   m 1 0   m*se 1 0 0 0   mrc_kffrq_log10_disc 0 0 0 1 0 0,
		'0 0 2 5'   intercept 1   se 1 0   em_wlen_norm_sml 0 0 1   m 1 0   m*se 1 0 0 0   mrc_kffrq_log10_disc 0 0 0 0 1 0,
		'0 0 2 6'   intercept 1   se 1 0   em_wlen_norm_sml 0 0 1   m 1 0   m*se 1 0 0 0   mrc_kffrq_log10_disc 0 0 0 0 0 1,
		'0 1 2 1'   intercept 1   se 1 0   em_wlen_norm_sml 0 0 1   m 0 1   m*se 0 0 1 0   mrc_kffrq_log10_disc 1 0 0 0 0 0,
		'0 1 2 2'   intercept 1   se 1 0   em_wlen_norm_sml 0 0 1   m 0 1   m*se 0 0 1 0   mrc_kffrq_log10_disc 0 1 0 0 0 0,
		'0 1 2 3'   intercept 1   se 1 0   em_wlen_norm_sml 0 0 1   m 0 1   m*se 0 0 1 0   mrc_kffrq_log10_disc 0 0 1 0 0 0,
		'0 1 2 4'   intercept 1   se 1 0   em_wlen_norm_sml 0 0 1   m 0 1   m*se 0 0 1 0   mrc_kffrq_log10_disc 0 0 0 1 0 0,
		'0 1 2 5'   intercept 1   se 1 0   em_wlen_norm_sml 0 0 1   m 0 1   m*se 0 0 1 0   mrc_kffrq_log10_disc 0 0 0 0 1 0,
		'0 1 2 6'   intercept 1   se 1 0   em_wlen_norm_sml 0 0 1   m 0 1   m*se 0 0 1 0   mrc_kffrq_log10_disc 0 0 0 0 0 1,


		'1 0 0 1'   intercept 1   se 0 1   em_wlen_norm_sml 1 0 0   m 1 0   m*se 0 1 0 0   mrc_kffrq_log10_disc 1 0 0 0 0 0,
		'1 0 0 2'   intercept 1   se 0 1   em_wlen_norm_sml 1 0 0   m 1 0   m*se 0 1 0 0   mrc_kffrq_log10_disc 0 1 0 0 0 0,
		'1 0 0 3'   intercept 1   se 0 1   em_wlen_norm_sml 1 0 0   m 1 0   m*se 0 1 0 0   mrc_kffrq_log10_disc 0 0 1 0 0 0,
		'1 0 0 4'   intercept 1   se 0 1   em_wlen_norm_sml 1 0 0   m 1 0   m*se 0 1 0 0   mrc_kffrq_log10_disc 0 0 0 1 0 0,
		'1 0 0 5'   intercept 1   se 0 1   em_wlen_norm_sml 1 0 0   m 1 0   m*se 0 1 0 0   mrc_kffrq_log10_disc 0 0 0 0 1 0,
		'1 0 0 6'   intercept 1   se 0 1   em_wlen_norm_sml 1 0 0   m 1 0   m*se 0 1 0 0   mrc_kffrq_log10_disc 0 0 0 0 0 1,
		'1 1 0 1'   intercept 1   se 0 1   em_wlen_norm_sml 1 0 0   m 0 1   m*se 0 0 0 1   mrc_kffrq_log10_disc 1 0 0 0 0 0,
		'1 1 0 2'   intercept 1   se 0 1   em_wlen_norm_sml 1 0 0   m 0 1   m*se 0 0 0 1   mrc_kffrq_log10_disc 0 1 0 0 0 0,
		'1 1 0 3'   intercept 1   se 0 1   em_wlen_norm_sml 1 0 0   m 0 1   m*se 0 0 0 1   mrc_kffrq_log10_disc 0 0 1 0 0 0,
		'1 1 0 4'   intercept 1   se 0 1   em_wlen_norm_sml 1 0 0   m 0 1   m*se 0 0 0 1   mrc_kffrq_log10_disc 0 0 0 1 0 0,
		'1 1 0 5'   intercept 1   se 0 1   em_wlen_norm_sml 1 0 0   m 0 1   m*se 0 0 0 1   mrc_kffrq_log10_disc 0 0 0 0 1 0,
		'1 1 0 6'   intercept 1   se 0 1   em_wlen_norm_sml 1 0 0   m 0 1   m*se 0 0 0 1   mrc_kffrq_log10_disc 0 0 0 0 0 1,

		'1 0 1 1'   intercept 1   se 0 1   em_wlen_norm_sml 0 1 0   m 1 0   m*se 0 1 0 0   mrc_kffrq_log10_disc 1 0 0 0 0 0,
		'1 0 1 2'   intercept 1   se 0 1   em_wlen_norm_sml 0 1 0   m 1 0   m*se 0 1 0 0   mrc_kffrq_log10_disc 0 1 0 0 0 0,
		'1 0 1 3'   intercept 1   se 0 1   em_wlen_norm_sml 0 1 0   m 1 0   m*se 0 1 0 0   mrc_kffrq_log10_disc 0 0 1 0 0 0,
		'1 0 1 4'   intercept 1   se 0 1   em_wlen_norm_sml 0 1 0   m 1 0   m*se 0 1 0 0   mrc_kffrq_log10_disc 0 0 0 1 0 0,
		'1 0 1 5'   intercept 1   se 0 1   em_wlen_norm_sml 0 1 0   m 1 0   m*se 0 1 0 0   mrc_kffrq_log10_disc 0 0 0 0 1 0,
		'1 0 1 6'   intercept 1   se 0 1   em_wlen_norm_sml 0 1 0   m 1 0   m*se 0 1 0 0   mrc_kffrq_log10_disc 0 0 0 0 0 1,
		'1 1 1 1'   intercept 1   se 0 1   em_wlen_norm_sml 0 1 0   m 0 1   m*se 0 0 0 1   mrc_kffrq_log10_disc 1 0 0 0 0 0,
		'1 1 1 2'   intercept 1   se 0 1   em_wlen_norm_sml 0 1 0   m 0 1   m*se 0 0 0 1   mrc_kffrq_log10_disc 0 1 0 0 0 0,
		'1 1 1 3'   intercept 1   se 0 1   em_wlen_norm_sml 0 1 0   m 0 1   m*se 0 0 0 1   mrc_kffrq_log10_disc 0 0 1 0 0 0,
		'1 1 1 4'   intercept 1   se 0 1   em_wlen_norm_sml 0 1 0   m 0 1   m*se 0 0 0 1   mrc_kffrq_log10_disc 0 0 0 1 0 0,
		'1 1 1 5'   intercept 1   se 0 1   em_wlen_norm_sml 0 1 0   m 0 1   m*se 0 0 0 1   mrc_kffrq_log10_disc 0 0 0 0 1 0,
		'1 1 1 6'   intercept 1   se 0 1   em_wlen_norm_sml 0 1 0   m 0 1   m*se 0 0 0 1   mrc_kffrq_log10_disc 0 0 0 0 0 1,

		'1 0 2 1'   intercept 1   se 0 1   em_wlen_norm_sml 0 0 1   m 1 0   m*se 0 1 0 0   mrc_kffrq_log10_disc 1 0 0 0 0 0,
		'1 0 2 2'   intercept 1   se 0 1   em_wlen_norm_sml 0 0 1   m 1 0   m*se 0 1 0 0   mrc_kffrq_log10_disc 0 1 0 0 0 0,
		'1 0 2 3'   intercept 1   se 0 1   em_wlen_norm_sml 0 0 1   m 1 0   m*se 0 1 0 0   mrc_kffrq_log10_disc 0 0 1 0 0 0,
		'1 0 2 4'   intercept 1   se 0 1   em_wlen_norm_sml 0 0 1   m 1 0   m*se 0 1 0 0   mrc_kffrq_log10_disc 0 0 0 1 0 0,
		'1 0 2 5'   intercept 1   se 0 1   em_wlen_norm_sml 0 0 1   m 1 0   m*se 0 1 0 0   mrc_kffrq_log10_disc 0 0 0 0 1 0,
		'1 0 2 6'   intercept 1   se 0 1   em_wlen_norm_sml 0 0 1   m 1 0   m*se 0 1 0 0   mrc_kffrq_log10_disc 0 0 0 0 0 1,
		'1 1 2 1'   intercept 1   se 0 1   em_wlen_norm_sml 0 0 1   m 0 1   m*se 0 0 0 1   mrc_kffrq_log10_disc 1 0 0 0 0 0,
		'1 1 2 2'   intercept 1   se 0 1   em_wlen_norm_sml 0 0 1   m 0 1   m*se 0 0 0 1   mrc_kffrq_log10_disc 0 1 0 0 0 0,
		'1 1 2 3'   intercept 1   se 0 1   em_wlen_norm_sml 0 0 1   m 0 1   m*se 0 0 0 1   mrc_kffrq_log10_disc 0 0 1 0 0 0,
		'1 1 2 4'   intercept 1   se 0 1   em_wlen_norm_sml 0 0 1   m 0 1   m*se 0 0 0 1   mrc_kffrq_log10_disc 0 0 0 1 0 0,
		'1 1 2 5'   intercept 1   se 0 1   em_wlen_norm_sml 0 0 1   m 0 1   m*se 0 0 0 1   mrc_kffrq_log10_disc 0 0 0 0 1 0,
		'1 1 2 6'   intercept 1   se 0 1   em_wlen_norm_sml 0 0 1   m 0 1   m*se 0 0 0 1   mrc_kffrq_log10_disc 0 0 0 0 0 1 / exp cl;
run;
proc means data=tmp mean std; var res; run;




* Classifier: Estimates (len: SML, frq: LH);
proc sort data=D_roi_ps1_a1_80; by em_wlen_norm_sml mrc_kffrq_log10_lh sub m se; run;
proc glimmix data=D_roi_ps1_a1_80 (where=(em_nf > 0 and em_wlen_norm > 0 and em_wlen_norm <= 11)) order=data;
	class sub m se em_wlen_norm_sml mrc_kffrq_log10_lh;
	model em_nf = m | se em_wlen_norm_sml mrc_kffrq_log10_lh / d=p link=log;
	*model em_tt = m | se em_wlen_norm_sml mrc_kffrq_log10_lh / d=logn link=identity;
	random int / sub=sub gcorr;
	random _residual_;

	* se m len frq;
	estimate
		'0 0 0 0'   intercept 1   se 1 0   em_wlen_norm_sml 1 0 0   m 1 0   m*se 1 0 0 0   mrc_kffrq_log10_lh 1 0,
		'0 0 0 1'   intercept 1   se 1 0   em_wlen_norm_sml 1 0 0   m 1 0   m*se 1 0 0 0   mrc_kffrq_log10_lh 0 1,
		'0 1 0 0'   intercept 1   se 1 0   em_wlen_norm_sml 1 0 0   m 0 1   m*se 0 0 1 0   mrc_kffrq_log10_lh 1 0,
		'0 1 0 1'   intercept 1   se 1 0   em_wlen_norm_sml 1 0 0   m 0 1   m*se 0 0 1 0   mrc_kffrq_log10_lh 0 1,

		'0 0 1 0'   intercept 1   se 1 0   em_wlen_norm_sml 0 1 0   m 1 0   m*se 1 0 0 0   mrc_kffrq_log10_lh 1 0,
		'0 0 1 1'   intercept 1   se 1 0   em_wlen_norm_sml 0 1 0   m 1 0   m*se 1 0 0 0   mrc_kffrq_log10_lh 0 1,
		'0 1 1 0'   intercept 1   se 1 0   em_wlen_norm_sml 0 1 0   m 0 1   m*se 0 0 1 0   mrc_kffrq_log10_lh 1 0,
		'0 1 1 1'   intercept 1   se 1 0   em_wlen_norm_sml 0 1 0   m 0 1   m*se 0 0 1 0   mrc_kffrq_log10_lh 0 1,

		'0 0 2 0'   intercept 1   se 1 0   em_wlen_norm_sml 0 0 1   m 1 0   m*se 1 0 0 0   mrc_kffrq_log10_lh 1 0,
		'0 0 2 1'   intercept 1   se 1 0   em_wlen_norm_sml 0 0 1   m 1 0   m*se 1 0 0 0   mrc_kffrq_log10_lh 0 1,
		'0 1 2 0'   intercept 1   se 1 0   em_wlen_norm_sml 0 0 1   m 0 1   m*se 0 0 1 0   mrc_kffrq_log10_lh 1 0,
		'0 1 2 1'   intercept 1   se 1 0   em_wlen_norm_sml 0 0 1   m 0 1   m*se 0 0 1 0   mrc_kffrq_log10_lh 0 1,


		'1 0 0 0'   intercept 1   se 0 1   em_wlen_norm_sml 1 0 0   m 1 0   m*se 0 1 0 0   mrc_kffrq_log10_lh 1 0,
		'1 0 0 1'   intercept 1   se 0 1   em_wlen_norm_sml 1 0 0   m 1 0   m*se 0 1 0 0   mrc_kffrq_log10_lh 0 1,
		'1 1 0 0'   intercept 1   se 0 1   em_wlen_norm_sml 1 0 0   m 0 1   m*se 0 0 0 1   mrc_kffrq_log10_lh 1 0,
		'1 1 0 1'   intercept 1   se 0 1   em_wlen_norm_sml 1 0 0   m 0 1   m*se 0 0 0 1   mrc_kffrq_log10_lh 0 1,

		'1 0 1 0'   intercept 1   se 0 1   em_wlen_norm_sml 0 1 0   m 1 0   m*se 0 1 0 0   mrc_kffrq_log10_lh 1 0,
		'1 0 1 1'   intercept 1   se 0 1   em_wlen_norm_sml 0 1 0   m 1 0   m*se 0 1 0 0   mrc_kffrq_log10_lh 0 1,
		'1 1 1 0'   intercept 1   se 0 1   em_wlen_norm_sml 0 1 0   m 0 1   m*se 0 0 0 1   mrc_kffrq_log10_lh 1 0,
		'1 1 1 1'   intercept 1   se 0 1   em_wlen_norm_sml 0 1 0   m 0 1   m*se 0 0 0 1   mrc_kffrq_log10_lh 0 1,

		'1 0 2 0'   intercept 1   se 0 1   em_wlen_norm_sml 0 0 1   m 1 0   m*se 0 1 0 0   mrc_kffrq_log10_lh 1 0,
		'1 0 2 1'   intercept 1   se 0 1   em_wlen_norm_sml 0 0 1   m 1 0   m*se 0 1 0 0   mrc_kffrq_log10_lh 0 1,
		'1 1 2 0'   intercept 1   se 0 1   em_wlen_norm_sml 0 0 1   m 0 1   m*se 0 0 0 1   mrc_kffrq_log10_lh 1 0,
		'1 1 2 1'   intercept 1   se 0 1   em_wlen_norm_sml 0 0 1   m 0 1   m*se 0 0 0 1   mrc_kffrq_log10_lh 0 1 / ilink cl;
run;
proc means data=tmp mean std; var res; run;




* Classifier: Plots (len: SML, frq: 1..6);
proc template;
	define statgraph Class_l3_f6;
	begingraph;
		layout lattice / columns=3 rows=2 columngutter=0 rowgutter=0 skipemptycells=true rowdatarange=union columndatarange=union;
			rowaxes;
				rowaxis / griddisplay=on gridattrs=(pattern=1) display=(label ticks tickvalues) label="NF-NZ" labelattrs=(weight=bold) offsetmin=0.1 offsetmax=0.1;
				rowaxis / griddisplay=on gridattrs=(pattern=1) display=(label ticks tickvalues) label="TT"    labelattrs=(weight=bold) offsetmin=0.1 offsetmax=0.1;
			endrowaxes;

			columnaxes;
				columnaxis / griddisplay=on gridattrs=(pattern=1) display=(label ticks tickvalues) labelattrs=(weight=bold) label="FRQ (LEN=S)" type=linear linearopts=(viewmin=1 viewmax=6) offsetmin=0.1 offsetmax=0.1;
				columnaxis / griddisplay=on gridattrs=(pattern=1) display=(label ticks tickvalues) labelattrs=(weight=bold) label="FRQ (LEN=M)" type=linear linearopts=(viewmin=1 viewmax=6) offsetmin=0.1 offsetmax=0.1;
				columnaxis / griddisplay=on gridattrs=(pattern=1) display=(label ticks tickvalues) labelattrs=(weight=bold) label="FRQ (LEN=L)" type=linear linearopts=(viewmin=1 viewmax=6) offsetmin=0.1 offsetmax=0.1;
			endcolumnaxes;

			layout overlay;
				bandplot x=frq limitlower=se1_m0_l0_nf_nz_se_l limitupper=se1_m0_l0_nf_nz_se_u / display=(fill outline) fillattrs=(color=cxeeeeee) outlineattrs=(thickness=1 color=cx999999); scatterplot x=frq y=se1_m0_l0_nf_nz_m / markerattrs=(symbol=circlefilled   size=0.1in color=cx0000ff); seriesplot y=se1_m0_l0_nf_nz_m x=frq / lineattrs=(thickness=2 color=cx0000ff);
				bandplot x=frq limitlower=se1_m1_l0_nf_nz_se_l limitupper=se1_m1_l0_nf_nz_se_u / display=(fill outline) fillattrs=(color=cxeeeeee) outlineattrs=(thickness=1 color=cx999999); scatterplot x=frq y=se1_m1_l0_nf_nz_m / markerattrs=(symbol=trianglefilled size=0.1in color=cxff0000); seriesplot y=se1_m1_l0_nf_nz_m x=frq / lineattrs=(thickness=2 color=cxff0000);
			endlayout;
			layout overlay;
				bandplot x=frq limitlower=se1_m0_l1_nf_nz_se_l limitupper=se1_m0_l1_nf_nz_se_u / display=(fill outline) fillattrs=(color=cxeeeeee) outlineattrs=(thickness=1 color=cx999999); scatterplot x=frq y=se1_m0_l1_nf_nz_m / markerattrs=(symbol=circlefilled   size=0.1in color=cx0000ff); seriesplot y=se1_m0_l1_nf_nz_m x=frq / lineattrs=(thickness=2 color=cx0000ff);
				bandplot x=frq limitlower=se1_m1_l1_nf_nz_se_l limitupper=se1_m1_l1_nf_nz_se_u / display=(fill outline) fillattrs=(color=cxeeeeee) outlineattrs=(thickness=1 color=cx999999); scatterplot x=frq y=se1_m1_l1_nf_nz_m / markerattrs=(symbol=trianglefilled size=0.1in color=cxff0000); seriesplot y=se1_m1_l1_nf_nz_m x=frq / lineattrs=(thickness=2 color=cxff0000);
			endlayout;
			layout overlay;
				bandplot x=frq limitlower=se1_m0_l2_nf_nz_se_l limitupper=se1_m0_l2_nf_nz_se_u / display=(fill outline) fillattrs=(color=cxeeeeee) outlineattrs=(thickness=1 color=cx999999); scatterplot x=frq y=se1_m0_l2_nf_nz_m / markerattrs=(symbol=circlefilled   size=0.1in color=cx0000ff); seriesplot y=se1_m0_l2_nf_nz_m x=frq / lineattrs=(thickness=2 color=cx0000ff);
				bandplot x=frq limitlower=se1_m1_l2_nf_nz_se_l limitupper=se1_m1_l2_nf_nz_se_u / display=(fill outline) fillattrs=(color=cxeeeeee) outlineattrs=(thickness=1 color=cx999999); scatterplot x=frq y=se1_m1_l2_nf_nz_m / markerattrs=(symbol=trianglefilled size=0.1in color=cxff0000); seriesplot y=se1_m1_l2_nf_nz_m x=frq / lineattrs=(thickness=2 color=cxff0000);
			endlayout;

			layout overlay;
				bandplot x=frq limitlower=se1_m0_l0_tt_se_l    limitupper=se1_m0_l0_tt_se_u    / display=(fill outline) fillattrs=(color=cxeeeeee) outlineattrs=(thickness=1 color=cx999999); scatterplot x=frq y=se1_m0_l0_tt_m    / markerattrs=(symbol=circlefilled   size=0.1in color=cx0000ff); seriesplot y=se1_m0_l0_tt_m    x=frq / lineattrs=(thickness=2 color=cx0000ff);
				bandplot x=frq limitlower=se1_m1_l0_tt_se_l    limitupper=se1_m1_l0_tt_se_u    / display=(fill outline) fillattrs=(color=cxeeeeee) outlineattrs=(thickness=1 color=cx999999); scatterplot x=frq y=se1_m1_l0_tt_m    / markerattrs=(symbol=trianglefilled size=0.1in color=cxff0000); seriesplot y=se1_m1_l0_tt_m    x=frq / lineattrs=(thickness=2 color=cxff0000);
			endlayout;
			layout overlay;
				bandplot x=frq limitlower=se1_m0_l1_tt_se_l    limitupper=se1_m0_l1_tt_se_u    / display=(fill outline) fillattrs=(color=cxeeeeee) outlineattrs=(thickness=1 color=cx999999); scatterplot x=frq y=se1_m0_l1_tt_m    / markerattrs=(symbol=circlefilled   size=0.1in color=cx0000ff); seriesplot y=se1_m0_l1_tt_m    x=frq / lineattrs=(thickness=2 color=cx0000ff);
				bandplot x=frq limitlower=se1_m1_l1_tt_se_l    limitupper=se1_m1_l1_tt_se_u    / display=(fill outline) fillattrs=(color=cxeeeeee) outlineattrs=(thickness=1 color=cx999999); scatterplot x=frq y=se1_m1_l1_tt_m    / markerattrs=(symbol=trianglefilled size=0.1in color=cxff0000); seriesplot y=se1_m1_l1_tt_m    x=frq / lineattrs=(thickness=2 color=cxff0000);
			endlayout;
			layout overlay;
				bandplot x=frq limitlower=se1_m0_l2_tt_se_l    limitupper=se1_m0_l2_tt_se_u    / display=(fill outline) fillattrs=(color=cxeeeeee) outlineattrs=(thickness=1 color=cx999999); scatterplot x=frq y=se1_m0_l2_tt_m    / markerattrs=(symbol=circlefilled   size=0.1in color=cx0000ff); seriesplot y=se1_m0_l2_tt_m    x=frq / lineattrs=(thickness=2 color=cx0000ff);
				bandplot x=frq limitlower=se1_m1_l2_tt_se_l    limitupper=se1_m1_l2_tt_se_u    / display=(fill outline) fillattrs=(color=cxeeeeee) outlineattrs=(thickness=1 color=cx999999); scatterplot x=frq y=se1_m1_l2_tt_m    / markerattrs=(symbol=trianglefilled size=0.1in color=cxff0000); seriesplot y=se1_m1_l2_tt_m    x=frq / lineattrs=(thickness=2 color=cxff0000);
			endlayout;
		endlayout;
	endgraph;
end;
run;
ods html style=journal gpath="&DATA_DIR\out\"; ods graphics / /* reset*/ width=10in height=5in /*ftext=swiss lfactor=1*/ imagename="class_len3_frq6" imagefmt=png border=off;
proc sgrender data=D_cls_l3_f6_ps1_a1_80 template=Class_l3_f6; run;




* Classifier: Plots (len: SML, frq: 1..6) [TT only];
proc template;
	define statgraph Class_l3_f6_TT;
	begingraph;
		layout lattice / columns=3 rows=1 columngutter=0 rowgutter=0 skipemptycells=true rowdatarange=union columndatarange=union;
			rowaxes;
				rowaxis / griddisplay=on gridattrs=(pattern=1) display=(label ticks tickvalues) label="TT_hat" labelattrs=(weight=bold) offsetmin=0.1 offsetmax=0.1;
			endrowaxes;

			columnaxes;
				columnaxis / griddisplay=on gridattrs=(pattern=1) display=(label ticks tickvalues) labelattrs=(weight=bold) label="FRQ (LEN=S)" type=linear linearopts=(viewmin=1 viewmax=6) offsetmin=0.1 offsetmax=0.1;
				columnaxis / griddisplay=on gridattrs=(pattern=1) display=(label ticks tickvalues) labelattrs=(weight=bold) label="FRQ (LEN=M)" type=linear linearopts=(viewmin=1 viewmax=6) offsetmin=0.1 offsetmax=0.1;
				columnaxis / griddisplay=on gridattrs=(pattern=1) display=(label ticks tickvalues) labelattrs=(weight=bold) label="FRQ (LEN=L)" type=linear linearopts=(viewmin=1 viewmax=6) offsetmin=0.1 offsetmax=0.1;
			endcolumnaxes;

			layout overlay;
				bandplot x=frq limitlower=se1_m0_l0_tt_se_l limitupper=se1_m0_l0_tt_se_u / display=(fill outline) fillattrs=(color=cxeeeeee) outlineattrs=(thickness=1 color=cx999999); scatterplot x=frq y=se1_m0_l0_tt_m / markerattrs=(symbol=trianglefilled size=0.125in color=cx000000);  seriesplot y=se1_m0_l0_tt_m x=frq / lineattrs=(thickness=2 color=cx000000);
				bandplot x=frq limitlower=se1_m1_l0_tt_se_l limitupper=se1_m1_l0_tt_se_u / display=(fill outline) fillattrs=(color=cxeeeeee) outlineattrs=(thickness=1 color=cx999999); scatterplot x=frq y=se1_m1_l0_tt_m / markerattrs=(symbol=circlefilled   size=0.125in color=cx000000);  seriesplot y=se1_m1_l0_tt_m x=frq / lineattrs=(thickness=2 color=cx000000);
				seriesplot x=frq y=se1_l0_tt_midp / lineattrs=(pattern=4 thickness=2 color=cx0000ff) name="mid-line";

				layout gridded / autoalign=(topleft);  discretelegend "mid-line" / border=false opaque=false valueattrs=(color=cxffffff);  endlayout;
				entry "class boundary" / autoalign=(topleft) pad=(left=0.50in top=0.00in) textattrs=(weight=bold);
				entry "relevant"       / autoalign=(topleft) pad=(left=2.15in top=2.25in) textattrs=(weight=bold);
				entry "non-relevant"   / autoalign=(topleft) pad=(left=0.25in top=3.50in) textattrs=(weight=bold);
			endlayout;
			layout overlay;
				bandplot x=frq limitlower=se1_m0_l1_tt_se_l limitupper=se1_m0_l1_tt_se_u / display=(fill outline) fillattrs=(color=cxeeeeee) outlineattrs=(thickness=1 color=cx999999); scatterplot x=frq y=se1_m0_l1_tt_m / markerattrs=(symbol=trianglefilled size=0.125in color=cx000000);  seriesplot y=se1_m0_l1_tt_m x=frq / lineattrs=(thickness=2 color=cx000000);
				bandplot x=frq limitlower=se1_m1_l1_tt_se_l limitupper=se1_m1_l1_tt_se_u / display=(fill outline) fillattrs=(color=cxeeeeee) outlineattrs=(thickness=1 color=cx999999); scatterplot x=frq y=se1_m1_l1_tt_m / markerattrs=(symbol=circlefilled   size=0.125in color=cx000000);  seriesplot y=se1_m1_l1_tt_m x=frq / lineattrs=(thickness=2 color=cx000000);
				seriesplot x=frq y=se1_l1_tt_midp / lineattrs=(pattern=4 thickness=2 color=cx0000ff);
			endlayout;
			layout overlay;
				bandplot x=frq limitlower=se1_m0_l2_tt_se_l limitupper=se1_m0_l2_tt_se_u / display=(fill outline) fillattrs=(color=cxeeeeee) outlineattrs=(thickness=1 color=cx999999); scatterplot x=frq y=se1_m0_l2_tt_m / markerattrs=(symbol=trianglefilled size=0.125in color=cx000000);  seriesplot y=se1_m0_l2_tt_m x=frq / lineattrs=(thickness=2 color=cx000000);
				bandplot x=frq limitlower=se1_m1_l2_tt_se_l limitupper=se1_m1_l2_tt_se_u / display=(fill outline) fillattrs=(color=cxeeeeee) outlineattrs=(thickness=1 color=cx999999); scatterplot x=frq y=se1_m1_l2_tt_m / markerattrs=(symbol=circlefilled   size=0.125in color=cx000000);  seriesplot y=se1_m1_l2_tt_m x=frq / lineattrs=(thickness=2 color=cx000000);
				seriesplot x=frq y=se1_l2_tt_midp / lineattrs=(pattern=4 thickness=2 color=cx0000ff);
			endlayout;
		endlayout;
	endgraph;
end;
run;
ods html style=journal gpath="&DATA_DIR\out\"; ods graphics / reset=all width=10in height=5in /*ftext=swiss lfactor=1*/ imagename="class_len3_frq6_tt" imagefmt=png border=off;
proc sgrender data=D_cls_l3_f6_ps1_a1_80 template=Class_l3_f6_TT; run;




* Classifier: Plots (len: SML, frq: LH);
proc template;
	define statgraph Class_l3_f2;
	begingraph;
		layout lattice / columns=3 rows=2 columngutter=0 rowgutter=0 skipemptycells=true rowdatarange=union columndatarange=union;
			rowaxes;
				rowaxis / griddisplay=on gridattrs=(pattern=1) display=(label ticks tickvalues) label="NF-NZ" labelattrs=(weight=bold) offsetmin=0.1 offsetmax=0.1;
				rowaxis / griddisplay=on gridattrs=(pattern=1) display=(label ticks tickvalues) label="TT"    labelattrs=(weight=bold) offsetmin=0.1 offsetmax=0.1;
			endrowaxes;

			columnaxes;
				columnaxis / griddisplay=on gridattrs=(pattern=1) display=(label ticks tickvalues) labelattrs=(weight=bold) label="FRQ (LEN=S)" type=discrete offsetmin=0.1 offsetmax=0.1;
				columnaxis / griddisplay=on gridattrs=(pattern=1) display=(label ticks tickvalues) labelattrs=(weight=bold) label="FRQ (LEN=M)" type=discrete offsetmin=0.1 offsetmax=0.1;
				columnaxis / griddisplay=on gridattrs=(pattern=1) display=(label ticks tickvalues) labelattrs=(weight=bold) label="FRQ (LEN=L)" type=discrete offsetmin=0.1 offsetmax=0.1;
			endcolumnaxes;

			layout overlay;
				scatterplot x=frq_str y=se1_m0_l0_nf_nz_m / yerrorlower=se1_m0_l0_nf_nz_se_l yerrorupper=se1_m0_l0_nf_nz_se_u markerattrs=(symbol=circlefilled   size=0.1in color=cx0000ff);  seriesplot x=frq_str y=se1_m0_l0_nf_nz_m / lineattrs=(thickness=2 color=cx0000ff);
				scatterplot x=frq_str y=se1_m1_l0_nf_nz_m / yerrorlower=se1_m1_l0_nf_nz_se_l yerrorupper=se1_m1_l0_nf_nz_se_u markerattrs=(symbol=trianglefilled size=0.1in color=cxff0000);  seriesplot x=frq_str y=se1_m1_l0_nf_nz_m / lineattrs=(thickness=2 color=cxff0000);
			endlayout;
			layout overlay;
				scatterplot x=frq_str y=se1_m0_l1_nf_nz_m / yerrorlower=se1_m0_l1_nf_nz_se_l yerrorupper=se1_m0_l1_nf_nz_se_u markerattrs=(symbol=circlefilled   size=0.1in color=cx0000ff);  seriesplot x=frq_str y=se1_m0_l1_nf_nz_m / lineattrs=(thickness=2 color=cx0000ff);
				scatterplot x=frq_str y=se1_m1_l1_nf_nz_m / yerrorlower=se1_m1_l1_nf_nz_se_l yerrorupper=se1_m1_l1_nf_nz_se_u markerattrs=(symbol=trianglefilled size=0.1in color=cxff0000);  seriesplot x=frq_str y=se1_m1_l1_nf_nz_m / lineattrs=(thickness=2 color=cxff0000);
			endlayout;
			layout overlay;
				scatterplot x=frq_str y=se1_m0_l2_nf_nz_m / yerrorlower=se1_m0_l2_nf_nz_se_l yerrorupper=se1_m0_l2_nf_nz_se_u markerattrs=(symbol=circlefilled   size=0.1in color=cx0000ff);  seriesplot x=frq_str y=se1_m0_l2_nf_nz_m / lineattrs=(thickness=2 color=cx0000ff);
				scatterplot x=frq_str y=se1_m1_l2_nf_nz_m / yerrorlower=se1_m1_l2_nf_nz_se_l yerrorupper=se1_m1_l2_nf_nz_se_u markerattrs=(symbol=trianglefilled size=0.1in color=cxff0000);  seriesplot x=frq_str y=se1_m1_l2_nf_nz_m / lineattrs=(thickness=2 color=cxff0000);
			endlayout;

			layout overlay;
				scatterplot x=frq_str y=se1_m0_l0_tt_m    / yerrorlower=se1_m0_l0_tt_se_l    yerrorupper=se1_m0_l0_tt_se_u    markerattrs=(symbol=circlefilled   size=0.1in color=cx0000ff);  seriesplot x=frq_str y=se1_m0_l0_tt_m    / lineattrs=(thickness=2 color=cx0000ff);
				scatterplot x=frq_str y=se1_m1_l0_tt_m    / yerrorlower=se1_m1_l0_tt_se_l    yerrorupper=se1_m1_l0_tt_se_u    markerattrs=(symbol=trianglefilled size=0.1in color=cxff0000);  seriesplot x=frq_str y=se1_m1_l0_tt_m    / lineattrs=(thickness=2 color=cxff0000);
			endlayout;
			layout overlay;
				scatterplot x=frq_str y=se1_m0_l1_tt_m    / yerrorlower=se1_m0_l1_tt_se_l    yerrorupper=se1_m0_l1_tt_se_u    markerattrs=(symbol=circlefilled   size=0.1in color=cx0000ff);  seriesplot x=frq_str y=se1_m0_l1_tt_m    / lineattrs=(thickness=2 color=cx0000ff);
				scatterplot x=frq_str y=se1_m1_l1_tt_m    / yerrorlower=se1_m1_l1_tt_se_l    yerrorupper=se1_m1_l1_tt_se_u    markerattrs=(symbol=trianglefilled size=0.1in color=cxff0000);  seriesplot x=frq_str y=se1_m1_l1_tt_m    / lineattrs=(thickness=2 color=cxff0000);
			endlayout;
			layout overlay;
				scatterplot x=frq_str y=se1_m0_l2_tt_m    / yerrorlower=se1_m0_l2_tt_se_l    yerrorupper=se1_m0_l2_tt_se_u    markerattrs=(symbol=circlefilled   size=0.1in color=cx0000ff);  seriesplot x=frq_str y=se1_m0_l2_tt_m    / lineattrs=(thickness=2 color=cx0000ff);
				scatterplot x=frq_str y=se1_m1_l2_tt_m    / yerrorlower=se1_m1_l2_tt_se_l    yerrorupper=se1_m1_l2_tt_se_u    markerattrs=(symbol=trianglefilled size=0.1in color=cxff0000);  seriesplot x=frq_str y=se1_m1_l2_tt_m    / lineattrs=(thickness=2 color=cxff0000);
			endlayout;
		endlayout;
	endgraph;
end;
run;
ods html style=journal gpath="&DATA_DIR\out\"; ods graphics / /* reset*/ width=5in height=5in /*ftext=swiss lfactor=1*/ imagename="class_len3_frq2" imagefmt=png border=off;
proc sgrender data=D_cls_l3_f2_ps1_a1_80 template=Class_l3_f2; run;




* Binary classifier: Performance evaluation (len: SML, frq: 1..6);

* (1) Prune the data to contain only sentence-terminal words with their characteristics;
data D_cls_l3_f6_ps1_a1_80_ev;
	set D_roi_ps1_a1_80;
	if (se = 1 and em_nf > 0);
	keep sub m se em_wlen_norm_sml mrc_kffrq_log10_disc em_tt;
run;


* (2) Check some statistics;
proc means data=D_cls_l3_f6_ps1_a1_80_ev;  class m;  run;  * N of all words;
proc means data=D_cls_l3_f6_ps1_a1_80_ev;  where (missing(mrc_kffrq_log10_disc));  class m;  run;  * N of words with missing frq ratings;


* (3) Print data that should be transposed into the decision criteria data set (I don't know how to do that and I don't have time to do that now in SAS);
data D_cls_l3_f6_ps1_a1_80_ev_dc;
	set D_cls_l3_f6_ps1_a1_80;
	keep se1_l0_tt_midp se1_l1_tt_midp se1_l2_tt_midp;
run;
proc print data=D_cls_l3_f6_ps1_a1_80_ev_dc;  run;  * copy and paste into a file


* (4) Load the decision criteria data set;
data D_cls_l3_f6_ps1_a1_80_ev_dc;
	infile "&DATA_DIR\data-class-len3-frq6-ps1-a1-80-ev-dc.txt" expandtabs firstobs=2 lrecl=2048;
	input em_wlen_norm_sml mrc_kffrq_log10_disc em_tt_midp;
run;


* (5) Combine the evaluation and decision criteria data sets;
proc sort data=D_cls_l3_f6_ps1_a1_80_ev;     by em_wlen_norm_sml mrc_kffrq_log10_disc;  run;
proc sort data=D_cls_l3_f6_ps1_a1_80_ev_dc;  by em_wlen_norm_sml mrc_kffrq_log10_disc;  run;

data D_cls_l3_f6_ps1_a1_80_ev;
	merge D_cls_l3_f6_ps1_a1_80_ev (where=(not missing(mrc_kffrq_log10_disc)) in=a) D_cls_l3_f6_ps1_a1_80_ev_dc(in=b);
	by em_wlen_norm_sml mrc_kffrq_log10_disc;
	if a and b;
run;


* (6) Assess the performance;
data D_cls_l3_f6_ps1_a1_80_ev;  set D_cls_l3_f6_ps1_a1_80_ev;  drop cls_res cls_tp cls_fn cls_fp cls_tn;  run;
data D_cls_l3_f6_ps1_a1_80_ev;
	set D_cls_l3_f6_ps1_a1_80_ev;

	* Calculate the classification result (correct or incorrect);
	cls_res = 0;
	if ((m = 0 and em_tt <= em_tt_midp - 200) or (m = 1 and em_tt >= em_tt_midp + 200)) then cls_res = 1;
	if (em_tt > em_tt_midp - 200  and em_tt < em_tt_midp + 200) then cls_res = .;

	* Calculate rates for confusion matrix;
	if (m = 0 and cls_res = 0) then do;  cls_fp = 0;  cls_tn = 1;  end;
	if (m = 0 and cls_res = 1) then do;  cls_fp = 1;  cls_tn = 0;  end;
	if (m = 1 and cls_res = 0) then do;  cls_tp = 0;  cls_fn = 1;  end;
	if (m = 1 and cls_res = 1) then do;  cls_tp = 1;  cls_fn = 0;  end;
run;

proc means data=D_cls_l3_f6_ps1_a1_80_ev print n mean sum;  var cls_res cls_tp cls_fn cls_fp cls_tn;  run;
proc means data=D_cls_l3_f6_ps1_a1_80_ev noprint n mean sum;  class em_wlen_norm_sml;  var cls_res cls_tp cls_fn cls_fp cls_tn;  run;

/*
Classifier: results
  I didn't take into account the fact, that the points falling in the separation band (no one's land) are simply discarded
	instead of being classified as "negative" automatically, therefore contributing to TNs and FPs.

  Separation = 5 ms
    Accuracy:    .43
    Sensitivity: .54
    Specificity: .41

  Separation = 30 ms
    Accuracy:    .43
    Sensitivity: .55
    Specificity: .40

  Separation = 60 ms
    Accuracy:    .49
    Sensitivity: .58
    Specificity: .47

  Separation = 100 ms
    Accuracy:    .55
    Sensitivity: .63
    Specificity: .53

  Separation = 200 ms
    Accuracy:    .91
    Sensitivity: .92
    Specificity: .91
*/




* Binary classifier: Performance evaluation (len: SML, frq: LH);

* (1) Prune the data to contain only sentence-terminal words with their characteristics;
data D_cls_l3_f2_ps1_a1_80_ev;
	set D_roi_ps1_a1_80;
	if (se = 1 and em_nf > 0);
	keep sub m se em_wlen_norm_sml mrc_kffrq_log10_lh em_tt;
run;


* (2) Check some statistics;
proc means data=D_cls_l3_f2_ps1_a1_80_ev;  class m;  run;  * N of all words;
proc means data=D_cls_l3_f2_ps1_a1_80_ev;  where (missing(mrc_kffrq_log10_lh));  class m;  run;  * N of words with missing frq ratings;


* (3) Print data that should be transposed into the decision criteria data set (I don't know how to do that and I don't have time to do that now in SAS);
data D_cls_l3_f2_ps1_a1_80_ev_dc;
	set D_cls_l3_f2_ps1_a1_80;
	keep se1_l0_tt_midp se1_l1_tt_midp se1_l2_tt_midp;
run;
proc print data=D_cls_l3_f2_ps1_a1_80_ev_dc;  run;  * copy and paste into a file


* (4) Load the decision criteria data set;
data D_cls_l3_f2_ps1_a1_80_ev_dc;
	infile "&DATA_DIR\data-class-len3-frq2-ps1-a1-80-ev-dc.txt" expandtabs firstobs=2 lrecl=2048;
	input em_wlen_norm_sml mrc_kffrq_log10_lh em_tt_midp;
run;


* (5) Combine the evaluation and decision criteria data sets;
proc sort data=D_cls_l3_f2_ps1_a1_80_ev;     by em_wlen_norm_sml mrc_kffrq_log10_lh;  run;
proc sort data=D_cls_l3_f2_ps1_a1_80_ev_dc;  by em_wlen_norm_sml mrc_kffrq_log10_lh;  run;

data D_cls_l3_f2_ps1_a1_80_ev;
	merge D_cls_l3_f2_ps1_a1_80_ev (where=(not missing(mrc_kffrq_log10_lh)) in=a) D_cls_l3_f2_ps1_a1_80_ev_dc(in=b);
	by em_wlen_norm_sml mrc_kffrq_log10_lh;
	if a and b;
run;


* (6) Assess the performance;
data D_cls_l3_f2_ps1_a1_80_ev_tmp;
	set D_cls_l3_f2_ps1_a1_80_ev;

	* Calculate the classification result (correct or incorrect);
	cls_res = 0;
	if ((m = 0 and em_tt <= em_tt_midp - 5) or (m = 1 and em_tt >= em_tt_midp + 5)) then cls_res = 1;
	if (em_tt > em_tt_midp - 5  and em_tt < em_tt_midp + 5) then cls_res = .;

	* Calculate rates for confusion matrix;
	if (m = 0 and cls_res = 0) then do;  cls_fp = 0;  cls_tn = 1;  end;
	if (m = 0 and cls_res = 1) then do;  cls_fp = 1;  cls_tn = 0;  end;
	if (m = 1 and cls_res = 0) then do;  cls_tp = 0;  cls_fn = 1;  end;
	if (m = 1 and cls_res = 1) then do;  cls_tp = 1;  cls_fn = 0;  end;
run;

proc means data=D_cls_l3_f2_ps1_a1_80_ev_tmp;  class em_wlen_norm_sml;  var cls_res;  run;
proc means data=D_cls_l3_f2_ps1_a1_80_ev_tmp;  class m em_wlen_norm_sml;  var cls_res;  run;
